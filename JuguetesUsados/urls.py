"""JuguetesUsados URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

#from apps.intercambio import views

urlpatterns = [
    path('',include('apps.usuarios.urls')),
    path('admin/', admin.site.urls),
    path('usuarios/', include('apps.usuarios.urls')),
    path('producto/', include('apps.producto.urls')),
    path('galeria/', include('apps.galeria.urls')),
    path('intercambio/', include('apps.intercambio.urls')),
    path('analisis/', include('apps.analisis.urls')),
    #path('productos/', include('apps.productos.urls')),
] + static (settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

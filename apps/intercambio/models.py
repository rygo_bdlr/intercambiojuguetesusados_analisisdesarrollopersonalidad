from django.db import models
from django.http import HttpResponseRedirect
from django.urls import reverse

from apps.producto.models import Producto,ProductoIntercambio
from apps.usuarios.models import Infante, Apoderado

# Create your models here.
class Publicacion(models.Model):
    #atributos
    descripcion=models.TextField(blank=True,null=True)
    Estado=models.CharField(max_length=40)

    #LLaves foraneas
    infante_autor = models.ForeignKey(Infante, on_delete=models.CASCADE, blank=True,null=True)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE, blank=True,null=True)

    def __str__(self):
        return self.producto.nombre

class DatosPublicacion(models.Model):
    #atributos
    dia = models.CharField(max_length=40)
    lugar = models.CharField(max_length=40)
    hora = models.CharField(max_length=40)

    #LLaves foraneas
    apoderado_autor = models.ForeignKey(Apoderado, on_delete=models.CASCADE, blank=True,null=True)
    publicacion = models.ForeignKey(Publicacion, on_delete=models.CASCADE, blank=True,null=True)

class PublicacionDeseada(models.Model):
    #LLaves foraneas
    infante_autor = models.ForeignKey(Infante, on_delete=models.CASCADE, blank=True,null=True)
    publicacion = models.ForeignKey(Publicacion, on_delete=models.CASCADE, blank=True,null=True)
    producto_a_dar= models.ForeignKey(Producto, on_delete=models.CASCADE, blank=True,null=True)

class Solicitud(models.Model):
    #atributos
    Estado=models.CharField(max_length=40)

    #LLaves foraneas
    publicacion = models.ForeignKey(Publicacion, on_delete=models.CASCADE, blank=True,null=True)
    apoderado_autor = models.ForeignKey(Apoderado, on_delete=models.CASCADE, blank=True,null=True)
    publicacion_deseada = models.ForeignKey(PublicacionDeseada, on_delete=models.CASCADE, blank=True,null=True)
    datod_publicacion= models.ForeignKey(DatosPublicacion, on_delete=models.CASCADE, blank=True,null=True)

class Cierre(models.Model):
    #atributos
    dia = models.CharField(max_length=40)
    lugar = models.CharField(max_length=40)
    hora = models.CharField(max_length=40)
    estado= models.CharField(max_length=40)


    #LLaves foraneas
    producto_a_dar_id= models.PositiveIntegerField()
    producto_a_recivir_id= models.PositiveIntegerField()
    apoderado_autor_rut = models.CharField(max_length=20)
    apoderado_destino_rut = models.CharField(max_length=20)
    unaSolicitud_id= models.PositiveIntegerField()

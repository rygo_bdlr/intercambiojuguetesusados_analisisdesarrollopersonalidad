from django.urls import path
from apps.intercambio import views

urlpatterns = [
    path('intercambio_in', views.verProductos, name='intercambio_in'),
    path('filtro', views.filtrar, name='filtro'),
    path('filtro_pag', views.filtrar_paginacion, name='filtro_pag'),
    path('publicar', views.publicar, name='publicar'),
    path('publicaciones/<int:id_infante>', views.publicaciones, name="publicaciones"),
    path('datosPublicacion/<int:id_publicacion>/<int:id_infante>', views.datosPublicacion, name="datosPublicacion"),
    path('subirDatosPublicacion/<int:id_publicacion>', views.subirDatosPublicacion, name="subirDatosPublicacion"),
    path('cargarDatosPublicacion', views.cargarDatosPublicacion, name='cargarDatosPublicacion'),
    path('verPublicacion', views.verPublicacion, name='verPublicacion'),
    path('verPublicacionIn', views.verPublicacionIn, name='verPublicacionIn'),
    path('desearPublicacion', views.desearPublicacion, name='desearPublicacion'),
    path('cancelarDesearPublicacion', views.cancelarDesearPublicacion, name='cancelarDesearPublicacion'),
    path('verDeseados/<int:id_infante>/<int:retorno>', views.verDeseados, name="verDeseados"),
    path('solicitar', views.solicitar, name='solicitar'),
    path('recibido/<int:id_infante>/<int:retorno>', views.solicitudRecividas, name='recibido'),
    path('respuesta>', views.respuesta, name='respuesta'),
    path('notificacionesIn>', views.notificacionesIn, name='notificacionesIn'),
    path('cierresPendientes>', views.cierresPendientes, name='cierresPendientes'),
    path('intercambios_a_concretar', views.intercambios_a_concretar, name='intercambios_a_concretar'),
    path('juguetesEnintercambios_a_concretar>', views.juguetesEnintercambios_a_concretar, name='juguetesEnintercambios_a_concretar'),
    path('respuestaRealizado', views.respuestaRealizado, name='respuestaRealizado'),
]

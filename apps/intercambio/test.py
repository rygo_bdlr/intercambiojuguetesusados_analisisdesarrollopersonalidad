from django.test import TestCase
from django.test import Client
import unittest

from apps.usuarios.models import Usuario, Apoderado, Infante, Entidad
from apps.intercambio.models import Publicacion, DatosPublicacion, PublicacionDeseada, Solicitud, Cierre
from apps.usuarios.models import Usuario, Apoderado, Entidad, Infante
from apps.producto.models import Producto,ProductoIntercambio, Historial
from apps.galeria.models import Galeria
from apps.analisis.models import Desarrollo, Preferencias

from django.urls import reverse

import json


class IntercambioTest(TestCase):

    @classmethod
    def setUp(self):

        self.usuario_ap= Usuario.objects.create(password="password", username="user_ap", tipo="AP")
        self.usuario_ap2= Usuario.objects.create(password="password", username="user_ap2", tipo="AP")
        self.usuario_in= Usuario.objects.create(password="password", username="user_in", tipo="IN")
        self.usuario_in2= Usuario.objects.create(password="password", username="user_in2", tipo="IN")
        self.usuario_en= Usuario.objects.create(password="password", username="user_en", tipo="EN")

        self.apoderado= Apoderado.objects.create(rut="1",nombre="apoderado",apellido="apellido",edad=1,movil=1,email="a@gmail.com",usuario=self.usuario_ap,foto="foto")
        self.apoderado2= Apoderado.objects.create(rut="2",nombre="apoderado2",apellido="apellido2",edad=1,movil=1,email="a2@gmail.com",usuario=self.usuario_ap2,foto="foto")
        self.infante= Infante.objects.create(nombre="niño",edad=1,apoderado=self.apoderado,usuario=self.usuario_in,foto="foto")
        self.infante2= Infante.objects.create(nombre="niño2",edad=1,apoderado=self.apoderado2,usuario=self.usuario_in2,foto="foto")
        self.galeria=Galeria.objects.create(limiteProductos=100,infante=self.infante)
        self.galeria2=Galeria.objects.create(limiteProductos=100,infante=self.infante2)
        self.entidad= Entidad.objects.create(nombre="entidad",email="e@gmail.com",pagina="pagina",usuario=self.usuario_en,foto="foto")

        self.desarrollo= Desarrollo.objects.create(AF=1,infante=self.infante,CR=1,IN=1,MO=1,SO=1)
        self.preferencias= Preferencias.objects.create(infante=self.infante,DV=1,LL=1,RM=1,MC=1,FM=1,PM=1,AV=1,TA=1,AA=1,HH=1,MM=1,CA=1,IV=1,MU=1,CT=1,PR=1,MA=1,VO=1,CC=1,PH=1,AE=1,OV=1,DA=1,TT=1)

        self.client= Client()



        ProductoIntercambio.objects.create(foto="foto",nombre="juguetePublicacion",categoria="DV",estado="deseado",id_publicacion=1,infante=self.infante2.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteDV",categoria="DV",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteLL",categoria="LL",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteRM",categoria="RM",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteMC",categoria="MC",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteFM",categoria="FM",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="juguetePM",categoria="PM",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteAV",categoria="AV",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteTA",categoria="TA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteAA",categoria="AA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteHH",categoria="HH",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteMM",categoria="MM",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteCA",categoria="CA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteIV",categoria="IV",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteMU",categoria="MU",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteCT",categoria="CT",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="juguetePR",categoria="PR",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteMA",categoria="MA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteVO",categoria="VO",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteCC",categoria="CC",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="juguetePH",categoria="PH",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteAE",categoria="AE",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteOV",categoria="OV",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteDA",categoria="DA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteTT",categoria="TT",estado="deseado",id_publicacion=1,infante=self.infante.id)

        self.productoPublicacion=Producto.objects.create(foto="foto",nombre="juguetePublicacion",categoria="DV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria)
        self.producto_a_darDV=Producto.objects.create(foto="foto",nombre="jugueteDV",categoria="DV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darLL=Producto.objects.create(foto="foto",nombre="jugueteLL",categoria="LL",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darRM=Producto.objects.create(foto="foto",nombre="jugueteRM",categoria="RM",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darMC=Producto.objects.create(foto="foto",nombre="jugueteMC",categoria="MC",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darFM=Producto.objects.create(foto="foto",nombre="jugueteFM",categoria="FM",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darPM=Producto.objects.create(foto="foto",nombre="juguetePM",categoria="PM",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darAV=Producto.objects.create(foto="foto",nombre="jugueteAV",categoria="AV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darTA=Producto.objects.create(foto="foto",nombre="jugueteTA",categoria="TA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darAA=Producto.objects.create(foto="foto",nombre="jugueteAA",categoria="AA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darHH=Producto.objects.create(foto="foto",nombre="jugueteHH",categoria="HH",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darMM=Producto.objects.create(foto="foto",nombre="jugueteMM",categoria="MM",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darCA=Producto.objects.create(foto="foto",nombre="jugueteCA",categoria="CA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darIV=Producto.objects.create(foto="foto",nombre="jugueteIV",categoria="IV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darMU=Producto.objects.create(foto="foto",nombre="jugueteMU",categoria="MU",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darCT=Producto.objects.create(foto="foto",nombre="jugueteCT",categoria="CT",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darPR=Producto.objects.create(foto="foto",nombre="juguetePR",categoria="PR",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darMA=Producto.objects.create(foto="foto",nombre="jugueteMA",categoria="MA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darVO=Producto.objects.create(foto="foto",nombre="jugueteVO",categoria="VO",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darCC=Producto.objects.create(foto="foto",nombre="jugueteCC",categoria="CC",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darPH=Producto.objects.create(foto="foto",nombre="juguetePH",categoria="PH",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darAE=Producto.objects.create(foto="foto",nombre="jugueteAE",categoria="AE",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darOV=Producto.objects.create(foto="foto",nombre="jugueteOV",categoria="OV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darDA=Producto.objects.create(foto="foto",nombre="jugueteDA",categoria="DA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darTT=Producto.objects.create(foto="foto",nombre="jugueteTT",categoria="TT",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)

        self.historialPublicacion=Historial.objects.create(producto=self.productoPublicacion,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialDV=Historial.objects.create(producto=self.producto_a_darDV,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialLL=Historial.objects.create(producto=self.producto_a_darLL,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialRM=Historial.objects.create(producto=self.producto_a_darRM,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialMC=Historial.objects.create(producto=self.producto_a_darMC,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialFM=Historial.objects.create(producto=self.producto_a_darFM,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialPM=Historial.objects.create(producto=self.producto_a_darPM,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialAV=Historial.objects.create(producto=self.producto_a_darAV,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialTA=Historial.objects.create(producto=self.producto_a_darTA,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialAA=Historial.objects.create(producto=self.producto_a_darAA,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialHH=Historial.objects.create(producto=self.producto_a_darHH,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialMM=Historial.objects.create(producto=self.producto_a_darMM,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialCA=Historial.objects.create(producto=self.producto_a_darCA,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialIV=Historial.objects.create(producto=self.producto_a_darIV,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialMU=Historial.objects.create(producto=self.producto_a_darMU,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialCT=Historial.objects.create(producto=self.producto_a_darCT,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialPR=Historial.objects.create(producto=self.producto_a_darPR,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialMA=Historial.objects.create(producto=self.producto_a_darMA,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialVO=Historial.objects.create(producto=self.producto_a_darVO,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialCC=Historial.objects.create(producto=self.producto_a_darCC,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialPH=Historial.objects.create(producto=self.producto_a_darPH,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialAE=Historial.objects.create(producto=self.producto_a_darAE,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialOV=Historial.objects.create(producto=self.producto_a_darOV,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialDA=Historial.objects.create(producto=self.producto_a_darDA,Registro="test",Adquisicion="test",Intercambiado=0)
        self.historialTT=Historial.objects.create(producto=self.producto_a_darTT,Registro="test",Adquisicion="test",Intercambiado=0)

        self.publicacion=Publicacion.objects.create(Estado="Publicado",producto=self.productoPublicacion,infante_autor=self.infante)

        self.datosPublicacion=DatosPublicacion.objects.create(dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",apoderado_autor=self.apoderado,publicacion=self.publicacion)

        self.publicacionDeseadaDV=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darDV)
        self.publicacionDeseadaLL=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darLL)
        self.publicacionDeseadaRM=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darRM)
        self.publicacionDeseadaMC=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMC)
        self.publicacionDeseadaFM=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darFM)
        self.publicacionDeseadaPM=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darPM)
        self.publicacionDeseadaAV=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAV)
        self.publicacionDeseadaTA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darTA)
        self.publicacionDeseadaAA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAA)
        self.publicacionDeseadaHH=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darHH)
        self.publicacionDeseadaMM=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMM)
        self.publicacionDeseadaCA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darCA)
        self.publicacionDeseadaIV=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darIV)
        self.publicacionDeseadaMU=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMU)
        self.publicacionDeseadaCT=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darCT)
        self.publicacionDeseadaPR=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darPR)
        self.publicacionDeseadaMA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMA)
        self.publicacionDeseadaVO=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darVO)
        self.publicacionDeseadaCC=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darCC)
        self.publicacionDeseadaPH=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darPH)
        self.publicacionDeseadaAE=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAE)
        self.publicacionDeseadaOV=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darOV)
        self.publicacionDeseadaDA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darDA)
        self.publicacionDeseadaTT=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darTT)

        self.solicitudDV=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaDV)
        self.solicitudLL=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaLL)
        self.solicitudRM=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaRM)
        self.solicitudMC=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMC)
        self.solicitudFM=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaFM)
        self.solicitudPM=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaPM)
        self.solicitudAV=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAV)
        self.solicitudTA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaTA)
        self.solicitudAA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAA)
        self.solicitudHH=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaHH)
        self.solicitudMM=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMM)
        self.solicitudCA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaCA)
        self.solicitudIV=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaIV)
        self.solicitudMU=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMU)
        self.solicitudCT=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaCT)
        self.solicitudPR=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaPR)
        self.solicitudMA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMA)
        self.solicitudVO=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaVO)
        self.solicitudCC=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaCC)
        self.solicitudPH=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaPH)
        self.solicitudAE=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAE)
        self.solicitudOV=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaOV)
        self.solicitudDA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaDA)
        self.solicitudTT=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaTT)

        # self.publicacionDeseadaDV2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darDV)
        # self.publicacionDeseadaLL2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darLL)
        # self.publicacionDeseadaRM2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darRM)
        # self.publicacionDeseadaMC2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMC)
        # self.publicacionDeseadaFM2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darFM)
        # self.publicacionDeseadaPM2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darPM)
        # self.publicacionDeseadaAV2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAV)
        # self.publicacionDeseadaTA2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darTA)
        # self.publicacionDeseadaAA2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAA)
        # self.publicacionDeseadaHH2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darHH)
        # self.publicacionDeseadaMM2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMM)
        # self.publicacionDeseadaCA2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darCA)
        # self.publicacionDeseadaIV2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darIV)
        # self.publicacionDeseadaMU2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMU)
        # self.publicacionDeseadaAV2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAV)
        # self.publicacionDeseadaCT2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darCT)
        # self.publicacionDeseadaPR2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darPR)
        # self.publicacionDeseadaMA2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMA)
        # self.publicacionDeseadaVO2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darVO)
        # self.publicacionDeseadaCC2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darCC)
        # self.publicacionDeseadaPH2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darPH)
        # self.publicacionDeseadaAE2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAE)
        # self.publicacionDeseadaOV2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darOV)
        # self.publicacionDeseadaDA2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darDA)
        # self.publicacionDeseadaTT2=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darTT)
        #
        # self.solicitudDV2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaDV)
        # self.solicitudLL2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaLL)
        # self.solicitudRM2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaRM)
        # self.solicitudMC2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMC)
        # self.solicitudFM2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaFM)
        # self.solicitudPM2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaPM)
        # self.solicitudAV2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAV)
        # self.solicitudTA2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaTA)
        # self.solicitudAA2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAA)
        # self.solicitudHH2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaHH)
        # self.solicitudMM2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMM)
        # self.solicitudCA2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaCA)
        # self.solicitudIV2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaIV)
        # self.solicitudMU2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMU)
        # self.solicitudAV2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAV)
        # self.solicitudCT2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaCT)
        # self.solicitudPR2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaPR)
        # self.solicitudMA2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMA)
        # self.solicitudVO2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaVO)
        # self.solicitudCC2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaCC)
        # self.solicitudPH2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaPH)
        # self.solicitudAE2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAE)
        # self.solicitudOV2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaOV)
        # self.solicitudDA2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaDA)
        # self.solicitudTT2=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaTT)

        self.cierreDV=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darDV.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreLL=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darLL.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreRM=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darRM.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreMC=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darMC.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreFM=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darFM.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierrePM=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darPM.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreAV=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darAV.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreTA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darTA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreAA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darAA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreHH=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darHH.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreMM=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darMM.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreCA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darCA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreIV=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darIV.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreMU=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darMU.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreCT=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darCT.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierrePR=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darPR.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreMA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darMA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreVO=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darVO.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreCC=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darCC.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierrePH=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darPH.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreAE=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darAE.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreOV=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darOV.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreDA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darDA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreTT=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darTT.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")

    @classmethod
    def tearDown(self):
        Usuario.objects.all().delete()
        Apoderado.objects.all().delete()
        Infante.objects.all().delete()
        Galeria.objects.all().delete()
        Entidad.objects.all().delete()
        Desarrollo.objects.all().delete()
        Preferencias.objects.all().delete()
        ProductoIntercambio.objects.all().delete()
        Producto.objects.all().delete()
        Publicacion.objects.all().delete()
        DatosPublicacion.objects.all().delete()
        PublicacionDeseada.objects.all().delete()
        Solicitud.objects.all().delete()
        Cierre.objects.all().delete()
        Historial.objects.all().delete()

    def testVerProductos(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session['bienv']=True
        session.save()

        response = self.client.get(reverse('intercambio_in'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'intercambio/intercambio_in.html')

    def testFiltrar_todo(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session.save()

        response = self.client.post(
            reverse('filtro'),
            {"filtro": 'todo'}
        )
        self.assertEqual(response.status_code, 200)

    def testFiltrar(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session.save()

        response = self.client.post(
            reverse('filtro'),
            {"filtro": 'DV'}
        )
        self.assertEqual(response.status_code, 200)

    def testPublicar(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session.save()

        response = self.client.post(
            reverse('publicar'),
            {"estado": 'publicar', "producto": self.producto_a_darTT.pk, "infante_id": self.infante.pk, "descripcion": 'test'}
        )
        self.assertEqual(response.status_code, 200)

    def testPublicar_cancelar(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session.save()

        response = self.client.post(
            reverse('publicar'),
            {"estado": 'cancelar', "producto": self.producto_a_darTT.pk, "infante_id": self.infante.pk, "descripcion": 'test'}
        )
        self.assertEqual(response.status_code, 200)

    def testPublicaciones(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        response = self.client.get(reverse('publicaciones',kwargs={"id_infante": self.infante.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'intercambio/publicaciones_ap.html')

    def testDatosPublicacion(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        response = self.client.get(reverse('datosPublicacion',kwargs={"id_publicacion": self.publicacion.pk,"id_infante": self.infante.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'intercambio/datos_publicacion.html')

    def testSubirDatosPublicacion(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()

        dia=['Miércoles 10 de Octubre de 2018', 'Miércoles 21 de Noviembre de 2018']
        lugar=['[["German Tenderini 1840, Renca, Región Metropolitana, Chile","Carlos Sage 760, Quinta Normal, Región Metropolitana, Chile"],["Claudio Arrau 9486, Pudahuel, Región Metropolitana, Chile"]]']
        hora=['[[["13:33","15:55"],["15:05","04:44"]],[["16:59"]]]']

        response = self.client.post(
            reverse('subirDatosPublicacion',kwargs={"id_publicacion": self.publicacion.pk}),
            {"id_producto": self.productoPublicacion.pk, "Dias[]": dia, "Lugares[]": lugar, "Horas[]": hora}
        )
        self.assertEqual(response.status_code, 200)

    def testCargarDatosPublicacion(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()

        response = self.client.post(
            reverse('cargarDatosPublicacion'),
            {"publicacion_id": self.publicacion.pk}
        )
        self.assertEqual(response.status_code, 200)

    def testVerPublicacion(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()

        response = self.client.post(
            reverse('verPublicacion'),
            {"idProducto": self.productoPublicacion.pk,"idInfante": self.infante.pk}
        )
        self.assertEqual(response.status_code, 200)

    def testVerPublicacionIn(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()

        response = self.client.post(
            reverse('verPublicacionIn'),
            {"idPublicacion": self.publicacion.pk,"idInfante": self.infante.pk}
        )
        self.assertEqual(response.status_code, 200)

    def testDesearPublicacion(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.pk
        session.save()

        productos_id=[self.producto_a_darDV.pk,self.producto_a_darLL.pk,self.producto_a_darRM.pk,self.producto_a_darMC.pk,self.producto_a_darFM.pk,self.producto_a_darPM.pk,self.producto_a_darAV.pk,self.producto_a_darTA.pk,self.producto_a_darAA.pk,self.producto_a_darHH.pk,self.producto_a_darMM.pk,self.producto_a_darCA.pk,self.producto_a_darIV.pk,self.producto_a_darMU.pk,self.producto_a_darCT.pk,self.producto_a_darPR.pk,self.producto_a_darMA.pk,self.producto_a_darVO.pk,self.producto_a_darCC.pk,self.producto_a_darPH.pk,self.producto_a_darAE.pk,self.producto_a_darOV.pk,self.producto_a_darDA.pk,self.producto_a_darTT.pk]


        response = self.client.post(
            reverse('desearPublicacion'),
            {"publicacionId": self.publicacion.pk,"infante_autor": self.infante.pk,"editar_estado":'0',"productos_a_dar[]":productos_id}
        )
        self.assertEqual(response.status_code, 200)

    def testCancelarDesearPublicacion(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.pk
        session.save()


        response = self.client.post(
            reverse('cancelarDesearPublicacion'),
            {"publicacionId": self.publicacion.pk,"infante_autor": self.infante.pk}
        )
        self.assertEqual(response.status_code, 200)

    def testVerDeseados_in(self):
        self.client.force_login(self.usuario_in2)
        session = self.client.session
        session['id']=self.usuario_in2.pk
        session.save()

        response = self.client.get(reverse('verDeseados',kwargs={"id_infante": self.infante2.pk,"retorno": 1}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'intercambio/deseados_in.html')

    def testVerDeseados_ap(self):
        self.client.force_login(self.usuario_ap2)
        session = self.client.session
        session['id']=self.usuario_ap2.pk
        session.save()

        response = self.client.get(reverse('verDeseados',kwargs={"id_infante": self.infante2.pk,"retorno": 0}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'intercambio/deseados_ap.html')

    def testSolicitar(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()


        response = self.client.post(
            reverse('solicitar'),
            {"Publicacion_id": self.publicacion.pk,"infanteAutor_id": self.infante2.pk,"datosPublicacion_id":self.datosPublicacion.pk}
        )
        self.assertEqual(response.status_code, 200)

    def testSolicitudRecividas_in(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.pk
        session.save()

        response = self.client.get(reverse('recibido',kwargs={"id_infante": self.infante.pk,"retorno": 1}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'intercambio/recivido_in.html')

    def testSolicitudRecividas_ap(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()

        response = self.client.get(reverse('recibido',kwargs={"id_infante": self.infante.pk,"retorno": 0}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'intercambio/recivido_ap.html')

    def testNotificacionesIn(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.pk
        session.save()


        response = self.client.post(
            reverse('notificacionesIn')
        )
        self.assertEqual(response.status_code, 200)

    def testRespuesta_aceptar(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.pk
        session.save()


        response = self.client.post(
            reverse('respuesta'),
            {"unaSolicitudId": self.solicitudDV.pk,"respues": '1'}
        )
        self.assertEqual(response.status_code, 200)

    def testRespuesta_cancelar(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.pk
        session.save()


        response = self.client.post(
            reverse('respuesta'),
            {"unaSolicitudId": self.solicitudDV.pk,"respues": '0'}
        )
        self.assertEqual(response.status_code, 200)

    def testCierresPendientes(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()


        response = self.client.post(
            reverse('cierresPendientes'),
            {"rutApoderado": self.apoderado.pk}
        )
        self.assertEqual(response.status_code, 200)

    def testIntercambios_a_concretar(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()

        response = self.client.get(reverse('intercambios_a_concretar'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'intercambio/concretar.html')

    def testJuguetesEnintercambios_a_concretar(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()


        response = self.client.post(
            reverse('juguetesEnintercambios_a_concretar')
        )
        self.assertEqual(response.status_code, 200)

    def testRespuestaRealizado_cancelar(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()

        cierres=[self.cierreDV.pk,self.cierreLL.pk,self.cierreRM.pk,self.cierreMC.pk,self.cierreFM.pk,self.cierrePM.pk,self.cierreAV.pk,self.cierreTA.pk,self.cierreAA.pk,self.cierreHH.pk,self.cierreMM.pk,self.cierreCA.pk,self.cierreIV.pk,self.cierreMU.pk,self.cierreCT.pk,self.cierrePR.pk,self.cierreMA.pk,self.cierreVO.pk,self.cierreCC.pk,self.cierrePH.pk,self.cierreAE.pk,self.cierreOV.pk,self.cierreDA.pk,self.cierreTT.pk]

        response = self.client.post(
            reverse('respuestaRealizado'),
            {"unaSolicitudId": self.solicitudDV.pk,"respues": '0',"cierreid[]":cierres}
        )
        self.assertEqual(response.status_code, 200)

    def testRespuestaRealizado_aceptar(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.pk
        session.save()

        cierres=[self.cierreDV.pk,self.cierreLL.pk,self.cierreRM.pk,self.cierreMC.pk,self.cierreFM.pk,self.cierrePM.pk,self.cierreAV.pk,self.cierreTA.pk,self.cierreAA.pk,self.cierreHH.pk,self.cierreMM.pk,self.cierreCA.pk,self.cierreIV.pk,self.cierreMU.pk,self.cierreCT.pk,self.cierrePR.pk,self.cierreMA.pk,self.cierreVO.pk,self.cierreCC.pk,self.cierrePH.pk,self.cierreAE.pk,self.cierreOV.pk,self.cierreDA.pk,self.cierreTT.pk]

        response = self.client.post(
            reverse('respuestaRealizado'),
            {"unaSolicitudId": self.solicitudDV.pk,"respues": '1',"cierreid[]":cierres}
        )
        self.assertEqual(response.status_code, 200)

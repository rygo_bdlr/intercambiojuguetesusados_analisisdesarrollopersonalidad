from django.contrib import admin
from apps.intercambio.models import Publicacion, DatosPublicacion, PublicacionDeseada, Solicitud, Cierre

# Register your models here.
@admin.register(Publicacion)
class AdminPublicacion(admin.ModelAdmin):
    list_display=('producto',)

@admin.register(DatosPublicacion)
class AdminDatosPublicacion(admin.ModelAdmin):
    list_display=('publicacion',)

@admin.register(PublicacionDeseada)
class AdminPublicacionDeseada(admin.ModelAdmin):
    list_display=('publicacion',)

@admin.register(Solicitud)
class AdminSolicitud(admin.ModelAdmin):
    list_display=('publicacion',)

@admin.register(Cierre)
class AdminCierre(admin.ModelAdmin):
    list_display=('dia',)

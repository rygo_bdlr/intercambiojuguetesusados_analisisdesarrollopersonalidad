from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.db.models import Q

from apps.intercambio.models import Publicacion, DatosPublicacion, PublicacionDeseada, Solicitud, Cierre
from apps.usuarios.models import Usuario, Apoderado, Entidad, Infante
from apps.producto.models import Producto,ProductoIntercambio, Historial
from apps.galeria.models import Galeria
from apps.analisis.models import Desarrollo, Preferencias

from datetime import datetime
from django.utils import formats

import json

import math

# Create your views here.

def verProductos(request):
    data={}
    data['infante']=Infante.objects.get(usuario_id=request.session.get('id'))
    data['bienv']=request.session.get('bienv')
    request.session['bienv']=False

    data['productos']=productos=Producto.objects.filter(Disponible_a_intercambio=True)

    # data['productosAunDisp']= datospubli= DatosPublicacion.objects.all()
    #
    # dataproductos=[]
    #
    # for dp in datospubli:
    #     dia=dp.dia.split(" ")[1]
    #     mes=dp.dia.split(" ")[3]
    #     if mes=="Enero":
    #         mes="01"
    #     elif mes=="Febrero":
    #         mes="02"
    #     elif mes=="Marzo":
    #         mes="03"
    #     elif mes=="Abril":
    #         mes="04"
    #     elif mes=="Mayo":
    #         mes="05"
    #     elif mes=="Junio":
    #         mes="06"
    #     elif mes=="Julio":
    #         mes="07"
    #     elif mes=="Agosto":
    #         mes="08"
    #     elif mes=="Septiembre":
    #         mes="09"
    #     elif mes=="Octubre":
    #         mes="10"
    #     elif mes=="Noviembre":
    #         mes="11"
    #     elif mes=="Diciembre":
    #         mes="12"
    #
    #
    #     ano=dp.dia.split(" ")[5]
    #     hora=dp.hora.split(":")[0]
    #     minuto=dp.hora.split(":")[1]
    #
    #
    #     ahora=datetime.now()
    #     ahoraAno=ahora.year
    #     ahoraMes=str(format(ahora.month, '02d'))
    #     ahoradia=str(format(ahora.day, '02d'))
    #     ahoraHora=str(format(ahora.hour, '02d'))
    #     ahoraMinuto=str(format(ahora.minute, '02d'))
    #
    #     ahoratotal=str(ahoraAno)+str(ahoraMes)+str(ahoradia)+str(ahoraHora)+str(ahoraMinuto)
    #     publitotal=str(ano)+str(mes)+str(dia)+str(hora)+str(minuto)
    #
    #     print("Dia: ",dia," Mes: ",mes," Año: ",ano," Hora: ",hora," Minuto: ",minuto)
    #     print("Dia: ",ahoradia," Mes: ",ahoraMes," Año: ",ahoraAno," Hora: ",ahoraHora," Minuto: ",ahoraMinuto)
    #     print("H: ",ahoratotal, "P: ",publitotal)
    #
    #     print("public: ",dp.publicacion.producto.nombre)
    #
    #     if int(ahoratotal)<int(publitotal):
    #         esta=False
    #         for d in dataproductos:
    #             if d == dp.publicacion.producto:
    #                 esta=True
    #         if esta==False:
    #             dataproductos.append(dp.publicacion.producto)
    #
    # data['productos']=dataproductos

    template_name='intercambio/intercambio_in.html'
    return render(request,template_name,data)

def publicacionesVigente():
    data={}
    data['productosAunDisp']= datospubli= DatosPublicacion.objects.all().order_by('-id')

    dataproductos=[]

    for dp in datospubli:
        dia=dp.dia.split(" ")[1]
        mes=dp.dia.split(" ")[3]
        if mes=="Enero":
            mes="01"
        elif mes=="Febrero":
            mes="02"
        elif mes=="Marzo":
            mes="03"
        elif mes=="Abril":
            mes="04"
        elif mes=="Mayo":
            mes="05"
        elif mes=="Junio":
            mes="06"
        elif mes=="Julio":
            mes="07"
        elif mes=="Agosto":
            mes="08"
        elif mes=="Septiembre":
            mes="09"
        elif mes=="Octubre":
            mes="10"
        elif mes=="Noviembre":
            mes="11"
        elif mes=="Diciembre":
            mes="12"


        ano=dp.dia.split(" ")[5]
        hora=dp.hora.split(":")[0]
        minuto=dp.hora.split(":")[1]


        ahora=datetime.now()
        ahoraAno=ahora.year
        ahoraMes=str(format(ahora.month, '02d'))
        ahoradia=str(format(ahora.day, '02d'))
        ahoraHora=str(format(ahora.hour, '02d'))
        ahoraMinuto=str(format(ahora.minute, '02d'))

        ahoratotal=str(ahoraAno)+str(ahoraMes)+str(ahoradia)+str(ahoraHora)+str(ahoraMinuto)
        publitotal=str(ano)+str(mes)+str(dia)+str(hora)+str(minuto)

        # print("Dia: ",dia," Mes: ",mes," Año: ",ano," Hora: ",hora," Minuto: ",minuto)
        # print("Dia: ",ahoradia," Mes: ",ahoraMes," Año: ",ahoraAno," Hora: ",ahoraHora," Minuto: ",ahoraMinuto)
        # print("H: ",ahoratotal, "P: ",publitotal)
        #
        # print("public: ",dp.publicacion.producto.nombre)

        if int(ahoratotal)<int(publitotal):
            esta=False
            for d in dataproductos:
                if d == dp.publicacion.producto:
                    esta=True
            if esta==False:
                dataproductos.append(dp.publicacion.producto)
    return dataproductos

def filtrar(request):
    response={}
    data=[]
    largo=0

    filtro=request.POST.get('filtro')
    infante=Infante.objects.get(usuario_id=request.session.get('id'))


    g=Galeria.objects.get(infante_id=Infante.objects.get(usuario_id=request.session.get('id')).id)

    productosVige=publicacionesVigente()

    productos=[]

    if filtro=='todo':
        for p in productosVige:
            if p.galeria_id!=g.id:
                productos.append(p)
    else:
        for p in productosVige:
            if p.galeria_id!=g.id and p.categoria==filtro:
                productos.append(p)

    poblacion={'DV':0,'LL':0,'RM':0,'MC':0,'FM':0,'PM':0,'AV':0,'TA':0,'AA':0,'HH':0,'MM':0,'CA':0,'IV':0,'MU':0,'AV':0,'CT':0,'PR':0,'MA':0,'VO':0,'CC':0,'PH':0,'AE':0,'OV':0,'DA':0,'TT':0}
    for p in productosVige:
        if p.galeria_id!=g.id:
            poblacion[p.categoria]+=1
    # if filtro=='todo':
    #     productos=Producto.objects.exclude(galeria_id=g.id).filter(Disponible_a_intercambio=True).order_by('-id')
    # else:
    #     productos=Producto.objects.exclude(galeria_id=g.id).filter(categoria=filtro,Disponible_a_intercambio=True).order_by('-id')

    count=0
    l=0
    for producto in productos:
        l=l+1
        # Productos ofrecidos previamente:
        try:
            p=Publicacion.objects.get(producto_id=producto.id)
            count=PublicacionDeseada.objects.filter(infante_autor_id=infante.id,publicacion_id=p.id).count()
        except Publicacion.DoesNotExist:
            p=None
            count=0

        if count==0:
            largo+=1
            data.append([producto.pk,producto.foto.url,producto.nombre,producto.categoria])


    paginas=math.ceil(largo/10)
    paginass=math.ceil(l/10)

    response["data"]=data
    response["largo"]=largo
    response["paginas"]=paginas
    response["largoPag"]=paginass
    response["filtro"]=filtro
    response["poblacion"]=poblacion

    return JsonResponse(response)

def filtrar_paginacion(request):
    response={}
    data=[]
    largo=0

    filtro=request.POST.get('filtro')
    infante=Infante.objects.get(usuario_id=request.session.get('id'))
    pagina=request.POST.get('pagina')
    largoPaginacion=request.POST.get('largo')
    cantidad=10

    if pagina=='0':
        inicio=0
        fin=cantidad
    else:
        inicio=int(pagina)*cantidad
        fin=inicio+cantidad


    g=Galeria.objects.get(infante_id=Infante.objects.get(usuario_id=request.session.get('id')).id)

    productosTodo=[]
    productosVige=publicacionesVigente()

    if filtro=='todo':
        for p in productosVige:
            if p.galeria.id!=g.id:
                productosTodo.append(p)


    else:
        for p in productosVige:
            if p.galeria.id!=g.id and p.categoria==filtro:
                productosTodo.append(p)

    for p in productosVige:
        print(p.nombre+" - ")

    print("\n")
    for p in productosTodo:
        print(p.nombre+" - ")

    print("\n")


    productos=productosTodo[inicio:fin]

    count=0
    noMostrar=0
    for producto in productos:
        # Productos ofrecidos previamente:
        try:
            p=Publicacion.objects.get(producto_id=producto.id)
            count=PublicacionDeseada.objects.filter(infante_autor_id=infante.id,publicacion_id=p.id).count()
        except Publicacion.DoesNotExist:
            p=None
            count=0

        if count==0:
            largo+=1
            data.append([producto.pk,producto.foto.url,producto.nombre,producto.categoria])
        else:
            noMostrar=noMostrar+1



    paginas=math.ceil((int(largoPaginacion)+noMostrar)/cantidad)
    nuevolargoPaginiacion=int(largoPaginacion)
    print(largoPaginacion)

    response["data"]=data
    response["pagina"]=pagina
    response["largo"]=largo
    response["largoPag"]=largoPaginacion
    response["paginas"]=paginas
    response["filtro"]=filtro


    return JsonResponse(response)

def publicar(request):
    response={}
    data=[]

    estado=request.POST.get('estado')
    id_producto=request.POST.get('producto')
    id_infante=request.POST.get('infante_id')

    if estado=='cancelar':
        Publicacion.objects.filter(producto=id_producto).delete()
        Producto.objects.filter(id=id_producto).update(Disponible_a_intercambio=False)

    else:
        descripcion=request.POST.get('descripcion')

        # Creamos publicacion
        publicacion=Publicacion()
        publicacion.infante_autor=Infante.objects.get(id=id_infante)
        publicacion.descripcion=descripcion
        publicacion.Estado='Por confirmar'
        publicacion.producto=Producto.objects.get(id=id_producto)
        publicacion.save()

    response["data"]=data

    return JsonResponse(response)

def publicaciones(request,id_infante):
    data={}

    if Apoderado.objects.get(usuario_id=request.session.get('id')).rut == Infante.objects.get(id=id_infante).apoderado_id:

        data['apoderado']=Apoderado.objects.get(usuario_id=request.session.get('id'))
        data['infante']=Infante.objects.get(id=id_infante)

        try:
            publicacion=data['publicaciones']=Publicacion.objects.filter(infante_autor_id=id_infante)
            idExiste=[]
            for p in publicacion:
                agregar=False
                try:
                    idDatos=data['datospublicaciones']=DatosPublicacion.objects.filter(publicacion=p.id)
                    for i in idDatos:
                        agregar=True
                except DatosPublicacion.DoesNotExist:
                    data['datospublicaciones']=None
                if agregar == True:
                    idExiste.append(p.id)


            data['existe']=idExiste
        except Publicacion.DoesNotExist:
            data['publicaciones']=None
            data['existe']=-1

        template_name='intercambio/publicaciones_ap.html'
        return render(request,template_name,data)
    else:
        return HttpResponseRedirect(reverse('inicio_ap'))

def datosPublicacion(request, id_publicacion, id_infante):
    data={}

    if Apoderado.objects.get(usuario_id=request.session.get('id')).rut == Infante.objects.get(id=id_infante).apoderado_id:
        data['infante']=Infante.objects.get(id=id_infante)
        data['apoderado']=Apoderado.objects.get(usuario_id=request.session.get('id'))
        data['publicacion']=Publicacion.objects.get(id=id_publicacion)

        template_name='intercambio/datos_publicacion.html'
        return render(request,template_name,data)
    else:
        return HttpResponseRedirect(reverse('inicio_ap'))

def subirDatosPublicacion(request,id_publicacion):
    response={}
    data=[]

    id_producto=request.POST.get('id_producto')
    dias=request.POST.getlist('Dias[]')
    lugares_list=request.POST.getlist('Lugares[]')
    lugares=json.loads(lugares_list[0])
    horas_list=request.POST.getlist('Horas[]')
    horas=json.loads(horas_list[0])



    #Creamos los datos de la publicacion:
    try:
        Existe=DatosPublicacion.objects.filter(publicacion=id_publicacion)
    except DatosPublicacion.DoesNotExist:
        Existe='None'

    if Existe != 'None':
        Existe.delete()


    lenD=len(dias)
    if lenD>0:
        #publicamos Juguete
        Publicacion.objects.filter(id=id_publicacion).update(Estado='Publicado')
        Producto.objects.filter(id=id_producto).update(Disponible_a_intercambio=True)
    else:
        Producto.objects.filter(id=id_producto).update(Disponible_a_intercambio=False)

    for i in range(0,lenD):
        lenL=len(lugares[i])
        # print(" -Dia: ",dias[i])
        for j in range(0,lenL):
            lenH=len(horas[i][j])
            # print("   Lugar:",lugares[i][j])
            for k in range(0,lenH):
                datosPublicacion=DatosPublicacion()
                datosPublicacion.dia=dias[i]
                datosPublicacion.lugar=lugares[i][j]
                datosPublicacion.hora=horas[i][j][k]
                datosPublicacion.apoderado_autor=Apoderado.objects.get(usuario_id=request.session.get('id'))
                datosPublicacion.publicacion=Publicacion.objects.get(id=id_publicacion)
                datosPublicacion.save()

    response["data"]=data

    return JsonResponse(response)

def cargarDatosPublicacion(request):
    response={}
    data=[]
    ids=[]

    id_publicacion=request.POST.get('publicacion_id')

    datosPublicacion=DatosPublicacion.objects.filter(publicacion=id_publicacion)

    dias=[]
    lugaresD=[]
    lugares=[]
    horasD=[]
    horasL=[]
    horas=[]

    for datos in datosPublicacion:
        d=l=h=0
        existeD=False
        existeL=False
        existeH=False

        for i in range(0,len(data)):
            if datos.dia == data[i][0]:
                existeD=True
                d=i
            for j in range(0,len(data[i])):
                if datos.lugar == data[i][j][0]:
                    existeL=True
                    l=j
                for k in range(0,len(data[i][j])):
                    if datos.hora == data[i][j][k]:
                        existeH=True
                        h=k
        if existeD==False:
            data.append([datos.dia,[datos.lugar,datos.hora]])
            ids.append([datos.id,[datos.id,datos.id]])
        else:
            if existeL==False:
                data[d].append([datos.lugar,datos.hora])
                ids[d].append([datos.id,datos.id])
            else:
                if existeH==False:
                    data[d][l].append(datos.hora)
                    ids[d][l].append(datos.id)

        # print(datos.dia," ",datos.lugar," ",datos.hora)

    # print("-------------------------")
    # print(data)
    # print("-------------------------")
    # print("")

    response["data"]=data
    response["ids"]=ids
    return JsonResponse(response)

def verPublicacion(request):
    response={}
    producto=[]
    publicacion=[]
    misProductos=[]
    productos_antes=[]

    id_producto=request.POST.get('idProducto')
    id_infante=request.POST.get('idInfante')

    prod=Producto.objects.get(id=id_producto)
    publi=Publicacion.objects.get(producto_id=prod.pk)
    misProd=Producto.objects.filter(galeria_id=Galeria.objects.get(infante_id=id_infante).id)

    for p in misProd:
        misProductos.append([p.pk,p.foto.url,p.nombre])

    producto.append([prod.pk,prod.foto.url,prod.nombre,prod.estado,prod.marca,prod.modelo,prod.serie,prod.anno,prod.tamano,prod.material])

    publicacion.append([publi.pk,publi.descripcion])

    # Productos ofrecidos previamente:
    p=Publicacion.objects.get(producto_id=id_producto)
    try:
        deseada=PublicacionDeseada.objects.filter(infante_autor_id=id_infante,publicacion_id=p.id)
    except PublicacionDeseada.DoesNotExist:
        deseada='None'

    if deseada != 'None':
        for datos in deseada:
            productos_antes.append(datos.producto_a_dar.id)


    response["producto"]=producto
    response["publicacion"]=publicacion
    response["misProductos"]=misProductos
    response["productos_antes"]=productos_antes

    return JsonResponse(response)

def verPublicacionIn(request):
    response={}
    producto=[]
    publicacion=[]
    misProductos=[]
    productos_antes=[]

    id_publicacion=request.POST.get('idPublicacion')
    id_infante=request.POST.get('idInfante')

    publi=Publicacion.objects.get(id=id_publicacion)
    prod=Producto.objects.get(id=publi.producto.id)
    misProd=Producto.objects.filter(galeria_id=Galeria.objects.get(infante_id=id_infante).id)

    for p in misProd:
        misProductos.append([p.pk,p.foto.url,p.nombre])

    producto.append([prod.pk,prod.foto.url,prod.nombre])
    publicacion.append([publi.pk,publi.descripcion])

    # Productos ofrecidos previamente:
    try:
        deseada=PublicacionDeseada.objects.filter(infante_autor_id=id_infante,publicacion_id=publi.id)
    except PublicacionDeseada.DoesNotExist:
        deseada='None'

    if deseada != 'None':
        for datos in deseada:
            productos_antes.append(datos.producto_a_dar.id)


    response["producto"]=producto
    response["publicacion"]=publicacion
    response["misProductos"]=misProductos
    response["productos_antes"]=productos_antes

    return JsonResponse(response)

def desearPublicacion(request):
    response={}
    data=[]

    id_infante_autor=request.POST.get('infante_autor')
    id_publicacion=request.POST.get('publicacionId')
    editar=request.POST.get('editar_estado')
    productos=request.POST.getlist('productos_a_dar[]')

    InfAutor=Infante.objects.get(id=id_infante_autor)
    publicacion=Publicacion.objects.get(id=id_publicacion)

    inf_solicitante=Infante.objects.get(usuario_id=request.session.get('id'))

    #aumentamos preferencias y desarrollo:
    if editar=='0':
        producto=Producto.objects.get(id=Publicacion.objects.get(id=id_publicacion).producto.id)
        # print(producto.nombre, producto.QR)
        if producto.categoria=='DV':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).DV
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(DV=cantidad)
        elif producto.categoria=='LL':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).LL
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(LL=cantidad)
        elif producto.categoria=='RM':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).RM
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(RM=cantidad)
        elif producto.categoria=='MC':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).MC
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(MC=cantidad)
        elif producto.categoria=='FM':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).FM
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(FM=cantidad)
        elif producto.categoria=='PM':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).PM
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(PM=cantidad)
        elif producto.categoria=='AV':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).AV
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(AV=cantidad)
        elif producto.categoria=='TA':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).TA
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(TA=cantidad)
        elif producto.categoria=='AA':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).AA
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(AA=cantidad)
        elif producto.categoria=='HH':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).HH
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(HH=cantidad)
        elif producto.categoria=='MM':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).MM
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(MM=cantidad)
        elif producto.categoria=='CA':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).CA
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(CA=cantidad)
        elif producto.categoria=='IV':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).IV
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(IV=cantidad)
        elif producto.categoria=='MU':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).MU
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(MU=cantidad)
        elif producto.categoria=='AV':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).AV
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(AV=cantidad)
        elif producto.categoria=='CT':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).CT
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(CT=cantidad)
        elif producto.categoria=='PR':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).PR
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(PR=cantidad)
        elif producto.categoria=='MA':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).MA
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(MA=cantidad)
        elif producto.categoria=='VO':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).VO
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(VO=cantidad)
        elif producto.categoria=='CC':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).CC
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(CC=cantidad)
        elif producto.categoria=='PH':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).PH
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(PH=cantidad)
        elif producto.categoria=='AE':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).AE
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(AE=cantidad)
        elif producto.categoria=='OV':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).OV
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(OV=cantidad)
        elif producto.categoria=='DA':
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).DA
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(DA=cantidad)
        else:
            cantidad=Preferencias.objects.get(infante_id=inf_solicitante.id).TT
            cantidad=cantidad+1
            Preferencias.objects.filter(infante_id=inf_solicitante.id).update(TT=cantidad)


        if producto.categoria=='MC' or producto.categoria=='PM' or producto.categoria=='IV' or producto.categoria=='CC':
            cantidad=Desarrollo.objects.get(infante_id=inf_solicitante.id).AF
            cantidad=cantidad+1
            desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante.id).update(AF=cantidad)

        if producto.categoria=='DV' or producto.categoria=='HH' or producto.categoria=='MM' or producto.categoria=='CA' or producto.categoria=='MU' or producto.categoria=='PR' or producto.categoria=='PH' or producto.categoria=='AE':
            cantidad=Desarrollo.objects.get(infante_id=inf_solicitante.id).MO
            cantidad=cantidad+1
            desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante.id).update(MO=cantidad)

        if producto.categoria=='LL' or producto.categoria=='HH' or producto.categoria=='MM' or producto.categoria=='CA' or producto.categoria=='CT' or producto.categoria=='PR' or producto.categoria=='MA' or producto.categoria=='VO' or producto.categoria=='AE':
            cantidad=Desarrollo.objects.get(infante_id=inf_solicitante).IN
            cantidad=cantidad+1
            desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante).update(IN=cantidad)

        if producto.categoria=='RM' or producto.categoria=='MC' or producto.categoria=='FM' or producto.categoria=='PM' or producto.categoria=='AV' or producto.categoria=='TA' or producto.categoria=='AA' or producto.categoria=='HH' or producto.categoria=='MM' or producto.categoria=='CA' or producto.categoria=='IV' or producto.categoria=='AV' or producto.categoria=='CT' or producto.categoria=='OV' or producto.categoria=='DA' or producto.categoria=='TT':
            cantidad=Desarrollo.objects.get(infante_id=inf_solicitante.id).CR
            cantidad=cantidad+1
            desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante.id).update(CR=cantidad)

        if producto.categoria=='MC' or producto.categoria=='FM' or producto.categoria=='PM' or producto.categoria=='IV' or producto.categoria=='MA' or producto.categoria=='CC' or producto.categoria=='DA' or producto.categoria=='TT':
            cantidad=Desarrollo.objects.get(infante_id=inf_solicitante.id).SO
            cantidad=cantidad+1
            desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante.id).update(SO=cantidad)

        # se registra el juguete como deseado
        jc_dar=ProductoIntercambio()
        jc_dar.foto=publicacion.producto.foto
        jc_dar.nombre=publicacion.producto.nombre
        jc_dar.categoria=publicacion.producto.categoria
        jc_dar.estado_d=publicacion.producto.estado
        jc_dar.marca=publicacion.producto.marca
        jc_dar.modelo=publicacion.producto.modelo
        jc_dar.serie=publicacion.producto.serie
        jc_dar.anno=publicacion.producto.anno
        jc_dar.tamano=publicacion.producto.tamano
        jc_dar.material=publicacion.producto.material
        jc_dar.estado='deseado'
        jc_dar.infante=inf_solicitante.id
        jc_dar.id_publicacion=publicacion.id
        jc_dar.save()
        # print("entro con el ",jc_dar.nombre)

    #Creamos el deseo de la publicacion:

    try:
        Existe=PublicacionDeseada.objects.filter(infante_autor_id=id_infante_autor,publicacion_id=id_publicacion)
    except PublicacionDeseada.DoesNotExist:
        Existe='None'

    if Existe != 'None':
        Existe.delete()

    for id_producto in productos:
        publicacionDeseada=PublicacionDeseada()
        publicacionDeseada.infante_autor=InfAutor
        publicacionDeseada.publicacion=publicacion
        p=Producto.objects.get(id=id_producto)
        publicacionDeseada.producto_a_dar=p
        publicacionDeseada.save()

        # registramos productos para analisis
        esta=0
        if editar=='1':
            esta=ProductoIntercambio.objects.filter(infante=InfAutor.id,id_publicacion=publicacion.id,estado='ofrecido',categoria=p.categoria,nombre=p.nombre,foto=p.foto).count()
        if esta==0:
            jc_dar=ProductoIntercambio()
            jc_dar.foto=p.foto
            jc_dar.nombre=p.nombre
            jc_dar.categoria=p.categoria
            jc_dar.estado_d=p.estado
            jc_dar.marca=p.marca
            jc_dar.modelo=p.modelo
            jc_dar.serie=p.serie
            jc_dar.anno=p.anno
            jc_dar.tamano=p.tamano
            jc_dar.material=p.material
            jc_dar.estado='ofrecido'
            jc_dar.infante=InfAutor.id
            jc_dar.id_publicacion=publicacion.id
            jc_dar.save()

    response["data"]=data

    return JsonResponse(response)

def cancelarDesearPublicacion(request):
    response={}
    data=[]

    id_infante_autor=request.POST.get('infante_autor')
    id_publicacion=request.POST.get('publicacionId')

    inf_solicitante=Infante.objects.get(usuario_id=request.session.get('id'))

    #cancelamos preferencias y desarrollo:
    producto=Producto.objects.get(id=Publicacion.objects.get(id=id_publicacion).producto.id)
    # print(producto.nombre, producto.QR, producto.categoria)
    if producto.categoria=='DV':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).DV
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(DV=cantidad)
    elif producto.categoria=='LL':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).LL
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(LL=cantidad)
    elif producto.categoria=='RM':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).RM
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(RM=cantidad)
    elif producto.categoria=='MC':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).MC
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(MC=cantidad)
    elif producto.categoria=='FM':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).FM
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(FM=cantidad)
    elif producto.categoria=='PM':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).PM
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(PM=cantidad)
    elif producto.categoria=='AV':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).AV
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(AV=cantidad)
    elif producto.categoria=='TA':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).TA
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(TA=cantidad)
    elif producto.categoria=='AA':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).AA
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(AA=cantidad)
    elif producto.categoria=='HH':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).HH
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(HH=cantidad)
    elif producto.categoria=='MM':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).MM
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(MM=cantidad)
    elif producto.categoria=='CA':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).CA
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(CA=cantidad)
    elif producto.categoria=='IV':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).IV
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(IV=cantidad)
    elif producto.categoria=='MU':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).MU
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(MU=cantidad)
    elif producto.categoria=='AV':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).AV
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(AV=cantidad)
    elif producto.categoria=='CT':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).CT
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(CT=cantidad)
    elif producto.categoria=='PR':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).PR
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(PR=cantidad)
    elif producto.categoria=='MA':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).MA
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(MA=cantidad)
    elif producto.categoria=='VO':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).VO
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(VO=cantidad)
    elif producto.categoria=='CC':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).CC
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(CC=cantidad)
    elif producto.categoria=='PH':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).PH
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(PH=cantidad)
    elif producto.categoria=='AE':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).AE
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(AE=cantidad)
    elif producto.categoria=='OV':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).OV
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(OV=cantidad)
    elif producto.categoria=='DA':
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).DA
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(DA=cantidad)
    else:
        cantidad=Preferencias.objects.get(infante_id=inf_solicitante).TT
        if cantidad>=1:
            cantidad=cantidad-1
        Preferencias.objects.filter(infante_id=inf_solicitante).update(TT=cantidad)


    if producto.categoria=='MC' or producto.categoria=='PM' or producto.categoria=='IV' or producto.categoria=='CC':
        cantidad=Desarrollo.objects.get(infante_id=inf_solicitante).AF
        if cantidad>=1:
            cantidad=cantidad-1
        desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante).update(AF=cantidad)

    if producto.categoria=='DV' or producto.categoria=='HH' or producto.categoria=='MM' or producto.categoria=='CA' or producto.categoria=='MU' or producto.categoria=='PR' or producto.categoria=='PH' or producto.categoria=='AE':
        cantidad=Desarrollo.objects.get(infante_id=inf_solicitante).MO
        if cantidad>=1:
            cantidad=cantidad-1
        desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante).update(MO=cantidad)

    if producto.categoria=='LL' or producto.categoria=='HH' or producto.categoria=='MM' or producto.categoria=='CA' or producto.categoria=='CT' or producto.categoria=='PR' or producto.categoria=='MA' or producto.categoria=='VO' or producto.categoria=='AE':
        cantidad=Desarrollo.objects.get(infante_id=inf_solicitante).IN
        if cantidad>=1:
            cantidad=cantidad-1
        desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante).update(IN=cantidad)

    if producto.categoria=='RM' or producto.categoria=='MC' or producto.categoria=='FM' or producto.categoria=='PM' or producto.categoria=='AV' or producto.categoria=='TA' or producto.categoria=='AA' or producto.categoria=='HH' or producto.categoria=='MM' or producto.categoria=='CA' or producto.categoria=='IV' or producto.categoria=='AV' or producto.categoria=='CT' or producto.categoria=='OV' or producto.categoria=='DA' or producto.categoria=='TT':
        cantidad=Desarrollo.objects.get(infante_id=inf_solicitante).CR
        if cantidad>=1:
            cantidad=cantidad-1
        desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante).update(CR=cantidad)

    if producto.categoria=='MC' or producto.categoria=='FM' or producto.categoria=='PM' or producto.categoria=='IV' or producto.categoria=='MA' or producto.categoria=='CC' or producto.categoria=='DA' or producto.categoria=='TT':
        cantidad=Desarrollo.objects.get(infante_id=inf_solicitante).SO
        if cantidad>=1:
            cantidad=cantidad-1
        desarrollo=Desarrollo.objects.filter(infante_id=inf_solicitante).update(SO=cantidad)

    ProductoIntercambio.objects.filter(id_publicacion=id_publicacion,infante=id_infante_autor).delete()

    PublicacionDeseada.objects.filter(infante_autor_id=id_infante_autor,publicacion_id=id_publicacion).delete()

    response["data"]=data
    return JsonResponse(response)

def verDeseados(request,id_infante,retorno):
    data={}

    dondeReturn=-1

    if retorno==0:
        apo=Apoderado.objects.get(usuario_id=request.session.get('id'))
        if Apoderado.objects.get(usuario_id=request.session.get('id')).rut != Infante.objects.get(id=id_infante).apoderado_id:
            dondeReturn=0
    else:
        infan=Infante.objects.get(id=id_infante)
        apo=Apoderado.objects.get(rut=infan.apoderado.rut)
        if Infante.objects.get(usuario_id=request.session.get('id')).id != id_infante:
            dondeReturn=1

    if dondeReturn == -1:
        data['apoderado']=apo
        data['infante']=Infante.objects.get(id=id_infante)
        data['publicaciones']=[]
        data['productos']=[]
        productosEnPublicacion=[]

        publicacionDeseada=data['publicacionesDeseadas']=PublicacionDeseada.objects.filter(infante_autor_id=id_infante)
        idExiste=[]
        idPublicacion=0
        publicaciones=[]

        agregado=False
        inicial=True

        final=publicacionDeseada.count()
        i=1
        for p in publicacionDeseada:
            if inicial==True:
                idPublicacion=p.publicacion_id
                inicial=False
            if p.publicacion_id !=idPublicacion and i<final:
                data['productos'].append([idPublicacion,productosEnPublicacion])
                data['publicaciones'].append(Publicacion.objects.get(id=idPublicacion))
                productosEnPublicacion=[]
                idPublicacion=p.publicacion_id
                productosEnPublicacion.append(Producto.objects.get(id=p.producto_a_dar.id))

            elif p.publicacion_id !=idPublicacion and i==final:
                # print(i," ",final," ",p.publicacion_id)
                data['productos'].append([idPublicacion,productosEnPublicacion])
                data['publicaciones'].append(Publicacion.objects.get(id=idPublicacion))
                productosEnPublicacion=[]
                idPublicacion=p.publicacion_id
                productosEnPublicacion.append(Producto.objects.get(id=p.producto_a_dar.id))
                data['productos'].append([idPublicacion,productosEnPublicacion])
                data['publicaciones'].append(Publicacion.objects.get(id=idPublicacion))

            elif i==final:

                # print("finaaal: ",i," ",final," ",p.publicacion_id)
                # print("entroo")
                productosEnPublicacion.append(Producto.objects.get(id=p.producto_a_dar.id))
                data['productos'].append([idPublicacion,productosEnPublicacion])
                data['publicaciones'].append(Publicacion.objects.get(id=idPublicacion))
            else:
                productosEnPublicacion.append(Producto.objects.get(id=p.producto_a_dar.id))
            i=i+1

        data['solicitudes']=[]
        pub_idAnterior=0
        for p in data['publicacionesDeseadas']:
            pub_id=p.publicacion_id
            if pub_id!=pub_idAnterior:
                data['solicitudes']=solicitud=Solicitud.objects.filter(publicacion_deseada_id=p.id,apoderado_autor=apo)
                countS=solicitud.count()
                if countS>0:
                    # print(Solicitud.objects.get(publicacion_deseada_id=p.id,apoderado_autor=apo).Estado)
                    idExiste.append([p.publicacion_id,solicitud.first().Estado])
            pub_idAnterior=pub_id
        data['existe']=idExiste

        if retorno==0:
            template_name='intercambio/deseados_ap.html'
        else:
            template_name='intercambio/deseados_in.html'

        return render(request,template_name,data)

    elif dondeReturn==0:
        return HttpResponseRedirect(reverse('inicio_ap'))
    else:
        return HttpResponseRedirect(reverse('intercambio_in'))


def solicitar(request):
    response={}
    data=[]

    id_datosPublicacion=request.POST.get('datosPublicacion_id')
    id_Publicacion=request.POST.get('Publicacion_id')
    id_infante_autor=request.POST.get('infanteAutor_id')

    apoderado=Apoderado.objects.get(usuario_id=request.session.get('id'))

    try:
        Existe=Solicitud.objects.filter(apoderado_autor_id=apoderado.rut,publicacion_id=id_Publicacion)
    except Solicitud.DoesNotExist:
        Existe='None'

    if Existe != 'None':
        Existe.delete()


    publicacionDeseada=PublicacionDeseada.objects.filter(infante_autor_id=id_infante_autor,publicacion_id=id_Publicacion)

    for pDeseada in publicacionDeseada:
        # print(pDeseada.id)

        solicitud=Solicitud()
        solicitud.Estado="En espera"

        p=Publicacion.objects.get(id=id_Publicacion)
        solicitud.publicacion=p

        solicitud.apoderado_autor=apoderado

        solicitud.publicacion_deseada=pDeseada

        d=DatosPublicacion.objects.get(id=id_datosPublicacion)
        solicitud.datod_publicacion=d

        solicitud.save()

    response["data"]=data

    return JsonResponse(response)


def solicitudRecividas(request,id_infante,retorno):
    data={}
    data['infante']=Infante.objects.get(id=id_infante)

    publicaciones=Publicacion.objects.filter(infante_autor_id=id_infante)

    recivida=[]
    for publicacion in publicaciones:

        solicitudes=Solicitud.objects.filter(publicacion=publicacion,Estado="En espera")
        # print(publicacion.id)
        for solicitud in solicitudes:
            esta=False
            for i in range(0,len(recivida)):
                if solicitud.apoderado_autor_id == recivida[i][5] and solicitud.publicacion_id==recivida[i][1]:
                    esta=True
                    recivida[i][0].append(solicitud.id)
                    recivida[i][6].append([solicitud.publicacion_deseada.producto_a_dar.id,solicitud.publicacion_deseada.producto_a_dar.foto.url,solicitud.publicacion_deseada.producto_a_dar.nombre])
            if esta==False:
                recivida.append([[solicitud.id],solicitud.publicacion_id,[solicitud.publicacion.producto.id,solicitud.publicacion.producto.foto.url,solicitud.publicacion.producto.nombre],solicitud.Estado,[solicitud.datod_publicacion.dia,solicitud.datod_publicacion.lugar,solicitud.datod_publicacion.hora],solicitud.apoderado_autor_id,[[solicitud.publicacion_deseada.producto_a_dar.id,solicitud.publicacion_deseada.producto_a_dar.foto.url,solicitud.publicacion_deseada.producto_a_dar.nombre]]])
                # recivida[j][0]=[id,id,id]
                # recivida[j][1]=publicacion_id
                # recivida[j][2]=[miJid,miJfoto,miJn]
                # recivida[j][3]=Estado
                # recivida[j][4]=[dia,lugar,hora]
                # recivida[j][5]=apoderado_autor_id
                # recivida[j][6]=[[jid,jfoto,jnombre],[jid,jfoto,jnombre],[jid,jfoto,jnombre]]

    # for r in recivida:
    #     print("-------")
    #     print(r)
    #     print("-------")

    data["recividas"]=recivida
    # print(data["recividas"])
    dondeReturn=-1
    if retorno==1:
        template_name='intercambio/recivido_in.html'
        if Infante.objects.get(usuario_id=request.session.get('id')).id != id_infante:
            dondeReturn=1
    else:
        template_name='intercambio/recivido_ap.html'
        if Apoderado.objects.get(usuario_id=request.session.get('id')).rut != Infante.objects.get(id=id_infante).apoderado_id:
            dondeReturn=0

    if dondeReturn==-1:
        return render(request,template_name,data)
    elif dondeReturn==0:
        return HttpResponseRedirect(reverse('inicio_ap'))
    else:
        return HttpResponseRedirect(reverse('intercambio_in'))





def notificacionesIn(request):
    solicitudes=0
    response={}
    data={}
    # Solicitudes recividas
    data['infante']=infante=Infante.objects.get(usuario_id=request.session.get('id'))

    publicaciones=Publicacion.objects.filter(infante_autor_id=infante.id)

    numRecividas=0
    for publicacion in publicaciones:

        solicitudes=Solicitud.objects.filter(publicacion=publicacion,Estado="En espera")
        n=solicitudes.count()
        if n >0:
            if solicitudes.first().Estado !="Rechazada":
                numRecividas=numRecividas+1

    solicitudes=numRecividas

    response["solicitudes"]=solicitudes
    return JsonResponse(response)

# def respuesta(request):
#     response={}
#     data=[]
#
#     id_unaSolicitud=request.POST.get('unaSolicitudId')
#     r=request.POST.get('respues')
#
#     ejemplar=Solicitud.objects.get(id=id_unaSolicitud)
#     solicitudes=Solicitud.objects.filter(apoderado_autor_id=ejemplar.apoderado_autor.rut,publicacion_id=ejemplar.publicacion.id)
#
#     if r=='0':
#         for solicitud in solicitudes:
#             s=Solicitud.objects.get(id=solicitud.id)
#             s.Estado="Rechazada"
#             s.save()
#     else:
#         for solicitud in solicitudes:
#             inf_publicador=solicitud.publicacion.infante_autor
#             jug_publicador=solicitud.publicacion.producto
#
#             inf_solicitante=solicitud.publicacion_deseada.infante_autor
#             jug_solicitante=solicitud.publicacion_deseada.producto_a_dar
#
#             break
#
#         # Intercambiamos Juguetes
#         # juguete publicador -> solicitante:
#         juguetePublicador=Producto.objects.get(id=jug_publicador.id,galeria=Galeria.objects.get(infante=inf_publicador))
#         print("inf Publicador: ",inf_publicador.nombre," juguete: ",jug_publicador.nombre)
#
#         jugueteNuevo=Producto()
#         jugueteNuevo.foto=jug_publicador.foto
#         jugueteNuevo.nombre=jug_publicador.nombre
#         jugueteNuevo.categoria=jug_publicador.categoria
#         jugueteNuevo.Disponible_a_intercambio=False
#         jugueteNuevo.galeria=jug_solicitante.galeria
#
#         jugueteNuevo.save()
#
#         jn=Producto.objects.get(id=jugueteNuevo.id)
#         jn.generate_qrcode()
#         filename = '/codigoQR/qrcode-%s.png' % jugueteNuevo.id
#         jn.QR = filename
#         jn.save()
#
#         # registramos juguete para cierre:
#         jc_dar=ProductoIntercambio()
#         jc_dar.foto=jugueteNuevo.foto
#         jc_dar.nombre=jugueteNuevo.nombre
#         jc_dar.categoria=jugueteNuevo.categoria
#         jc_dar.save()
#
#         # Creamos Historial
#         historialAnterior=Historial.objects.get(producto=jug_publicador)
#
#         historial=Historial()
#         reg=datetime.now()
#         reg_formato=formats.date_format(reg,"DATE_FORMAT")
#         historial.Registro=historialAnterior.Registro
#         historial.Adquisicion=reg_formato
#         n=historialAnterior.Intercambiado+1
#         historial.Intercambiado=n
#         historial.producto=Producto.objects.get(id=jn.id)
#         historial.save()
#
#
#
#         # juguetePublicador.galeria=Galeria.objects.get(infante=inf_solicitante)
#         # juguetePublicador.save()
#
#         print("inf solicitante: ",inf_solicitante.nombre," juguetes:")
#         juguetes_a_borrar=[]
#         for solicitud in solicitudes:
#
#             jug_solicitante=solicitud.publicacion_deseada.producto_a_dar
#             jugueteSolicitante=Producto.objects.get(id=jug_solicitante.id,galeria=Galeria.objects.get(infante=inf_solicitante))
#             print(jug_solicitante.nombre)
#
#             jugueteNuevo=Producto()
#             jugueteNuevo.foto=jug_solicitante.foto
#             jugueteNuevo.nombre=jug_solicitante.nombre
#             jugueteNuevo.categoria=jug_solicitante.categoria
#             jugueteNuevo.Disponible_a_intercambio=False
#             jugueteNuevo.galeria=jug_publicador.galeria
#
#             jugueteNuevo.save()
#
#             jn=Producto.objects.get(id=jugueteNuevo.id)
#             jn.generate_qrcode()
#             filename = '/codigoQR/qrcode-%s.png' % jugueteNuevo.id
#             jn.QR = filename
#             jn.save()
#
#             # registramos juguete para cierre:
#             jc=ProductoIntercambio()
#             jc.foto=jugueteNuevo.foto
#             jc.nombre=jugueteNuevo.nombre
#             jc.categoria=jugueteNuevo.categoria
#             jc.save()
#
#             # registramos cierre
#             cierre=Cierre()
#             cierre.dia=solicitud.datod_publicacion.dia
#             cierre.lugar=solicitud.datod_publicacion.lugar
#             cierre.hora=solicitud.datod_publicacion.hora
#             cierre.producto_a_dar_id=jc_dar.id
#             cierre.producto_a_recivir_id=jc.id
#             cierre.apoderado_autor_rut=ejemplar.publicacion.infante_autor.apoderado.rut
#             cierre.apoderado_destino_rut=solicitud.apoderado_autor.rut
#             cierre.save()
#
#             # Creamos Historial
#
#             historialAnterior=Historial.objects.get(producto=jug_solicitante)
#
#             historial=Historial()
#             reg=datetime.now()
#             reg_formato=formats.date_format(reg,"DATE_FORMAT")
#             historial.Registro=historialAnterior.Registro
#             historial.Adquisicion=reg_formato
#             n=historialAnterior.Intercambiado+1
#             historial.Intercambiado=n
#             historial.producto=Producto.objects.get(id=jn.id)
#             historial.save()
#
#
#             juguetes_a_borrar.append(jugueteSolicitante.id)
#
#         for b in juguetes_a_borrar:
#             p=Producto.objects.get(id=b)
#             p.delete()
#
#         juguetePublicador.delete()
#
#             # jugueteSolicitante.galeria=Galeria.objects.get(infante=inf_publicador)
#             # jugueteSolicitante.save()
#
#     response["data"]=r
#     return JsonResponse(response)

def respuesta(request):
    response={}
    data=[]

    id_unaSolicitud=request.POST.get('unaSolicitudId')
    r=request.POST.get('respues')

    ejemplar=Solicitud.objects.get(id=id_unaSolicitud)
    solicitudes=Solicitud.objects.filter(apoderado_autor_id=ejemplar.apoderado_autor.rut,publicacion_id=ejemplar.publicacion.id)

    if r=='0':
        for solicitud in solicitudes:
            s=Solicitud.objects.get(id=solicitud.id)
            s.Estado="Rechazada"
            s.save()
    else:

        for solicitud in solicitudes:
            inf_publicador=solicitud.publicacion.infante_autor
            jug_publicador=solicitud.publicacion.producto
            id_infante_autor=inf_publicador.id

            inf_solicitante=solicitud.publicacion_deseada.infante_autor
            jug_solicitante=solicitud.publicacion_deseada.producto_a_dar

            jP=ProductoIntercambio.objects.get(id_publicacion=solicitud.publicacion.id,foto=jug_publicador.foto,nombre=jug_publicador.nombre,categoria=jug_publicador.categoria)
            break

        # Intercambiamos Juguetes
        # juguete publicador -> solicitante:
        juguetePublicador=Producto.objects.get(id=jug_publicador.id,galeria=Galeria.objects.get(infante=inf_publicador))
        # print("inf Publicador: ",inf_publicador.nombre," juguete: ",jug_publicador.nombre)

        # jugueteNuevo=Producto()
        # jugueteNuevo.foto=jug_publicador.foto
        # jugueteNuevo.nombre=jug_publicador.nombre
        # jugueteNuevo.categoria=jug_publicador.categoria
        # jugueteNuevo.Disponible_a_intercambio=False
        # jugueteNuevo.galeria=jug_solicitante.galeria
        #
        # jugueteNuevo.save()
        #
        # jn=Producto.objects.get(id=jugueteNuevo.id)
        # jn.generate_qrcode()
        # filename = '/codigoQR/qrcode-%s.png' % jugueteNuevo.id
        # jn.QR = filename
        # jn.save()



        # # Creamos Historial
        # historialAnterior=Historial.objects.get(producto=jug_publicador)
        #
        # historial=Historial()
        # reg=datetime.now()
        # reg_formato=formats.date_format(reg,"DATE_FORMAT")
        # historial.Registro=historialAnterior.Registro
        # historial.Adquisicion=reg_formato
        # n=historialAnterior.Intercambiado+1
        # historial.Intercambiado=n
        # historial.producto=Producto.objects.get(id=jn.id)
        # historial.save()



        # juguetePublicador.galeria=Galeria.objects.get(infante=inf_solicitante)
        # juguetePublicador.save()

        # print("inf solicitante: ",inf_solicitante.nombre," juguetes:")
        juguetes_a_borrar=[]
        for solicitud in solicitudes:

            jug_solicitante=solicitud.publicacion_deseada.producto_a_dar
            jugueteSolicitante=Producto.objects.get(id=jug_solicitante.id,galeria=Galeria.objects.get(infante=inf_solicitante))
            # print(jug_solicitante.nombre)

            # jugueteNuevo=Producto()
            # jugueteNuevo.foto=jug_solicitante.foto
            # jugueteNuevo.nombre=jug_solicitante.nombre
            # jugueteNuevo.categoria=jug_solicitante.categoria
            # jugueteNuevo.Disponible_a_intercambio=False
            # jugueteNuevo.galeria=jug_publicador.galeria
            #
            # jugueteNuevo.save()
            #
            # jn=Producto.objects.get(id=jugueteNuevo.id)
            # jn.generate_qrcode()
            # filename = '/codigoQR/qrcode-%s.png' % jugueteNuevo.id
            # jn.QR = filename
            # jn.save()



            jS=ProductoIntercambio.objects.get(id_publicacion=solicitud.publicacion.id,foto=jugueteSolicitante.foto,nombre=jugueteSolicitante.nombre,categoria=jugueteSolicitante.categoria)

            # registramos cierre
            cierre=Cierre()
            cierre.dia=solicitud.datod_publicacion.dia
            cierre.lugar=solicitud.datod_publicacion.lugar
            cierre.hora=solicitud.datod_publicacion.hora
            cierre.estado="A concretar"
            cierre.producto_a_dar_id=jP.id
            cierre.producto_a_recivir_id=jS.id
            cierre.apoderado_autor_rut=ejemplar.publicacion.infante_autor.apoderado.rut
            cierre.apoderado_destino_rut=solicitud.apoderado_autor.rut
            cierre.unaSolicitud_id=id_unaSolicitud
            cierre.save()


            #aumentamos preferencias y desarrollo:

            producto=jugueteSolicitante
            if producto.categoria=='DV':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).DV
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(DV=cantidad)
            elif producto.categoria=='LL':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).LL
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(LL=cantidad)
            elif producto.categoria=='RM':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).RM
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(RM=cantidad)
            elif producto.categoria=='MC':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).MC
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(MC=cantidad)
            elif producto.categoria=='FM':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).FM
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(FM=cantidad)
            elif producto.categoria=='PM':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).PM
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(PM=cantidad)
            elif producto.categoria=='AV':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).AV
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(AV=cantidad)
            elif producto.categoria=='TA':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).TA
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(TA=cantidad)
            elif producto.categoria=='AA':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).AA
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(AA=cantidad)
            elif producto.categoria=='HH':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).HH
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(HH=cantidad)
            elif producto.categoria=='MM':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).MM
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(MM=cantidad)
            elif producto.categoria=='CA':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).CA
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(CA=cantidad)
            elif producto.categoria=='IV':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).IV
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(IV=cantidad)
            elif producto.categoria=='MU':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).MU
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(MU=cantidad)
            elif producto.categoria=='AV':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).AV
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(AV=cantidad)
            elif producto.categoria=='CT':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).CT
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(CT=cantidad)
            elif producto.categoria=='PR':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).PR
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(PR=cantidad)
            elif producto.categoria=='MA':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).MA
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(MA=cantidad)
            elif producto.categoria=='VO':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).VO
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(VO=cantidad)
            elif producto.categoria=='CC':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).CC
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(CC=cantidad)
            elif producto.categoria=='PH':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).PH
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(PH=cantidad)
            elif producto.categoria=='AE':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).AE
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(AE=cantidad)
            elif producto.categoria=='OV':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).OV
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(OV=cantidad)
            elif producto.categoria=='DA':
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).DA
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(DA=cantidad)
            else:
                cantidad=Preferencias.objects.get(infante_id=id_infante_autor).TT
                cantidad=cantidad+1
                Preferencias.objects.filter(infante_id=id_infante_autor).update(TT=cantidad)


            if producto.categoria=='MC' or producto.categoria=='PM' or producto.categoria=='IV' or producto.categoria=='CC':
                cantidad=Desarrollo.objects.get(infante_id=id_infante_autor).AF
                cantidad=cantidad+1
                desarrollo=Desarrollo.objects.filter(infante_id=id_infante_autor).update(AF=cantidad)

            if producto.categoria=='DV' or producto.categoria=='HH' or producto.categoria=='MM' or producto.categoria=='CA' or producto.categoria=='MU' or producto.categoria=='PR' or producto.categoria=='PH' or producto.categoria=='AE':
                cantidad=Desarrollo.objects.get(infante_id=id_infante_autor).MO
                cantidad=cantidad+1
                desarrollo=Desarrollo.objects.filter(infante_id=id_infante_autor).update(MO=cantidad)

            if producto.categoria=='LL' or producto.categoria=='HH' or producto.categoria=='MM' or producto.categoria=='CA' or producto.categoria=='CT' or producto.categoria=='PR' or producto.categoria=='MA' or producto.categoria=='VO' or producto.categoria=='AE':
                cantidad=Desarrollo.objects.get(infante_id=id_infante_autor).IN
                cantidad=cantidad+1
                desarrollo=Desarrollo.objects.filter(infante_id=id_infante_autor).update(IN=cantidad)

            if producto.categoria=='RM' or producto.categoria=='MC' or producto.categoria=='FM' or producto.categoria=='PM' or producto.categoria=='AV' or producto.categoria=='TA' or producto.categoria=='AA' or producto.categoria=='HH' or producto.categoria=='MM' or producto.categoria=='CA' or producto.categoria=='IV' or producto.categoria=='AV' or producto.categoria=='CT' or producto.categoria=='OV' or producto.categoria=='DA' or producto.categoria=='TT':
                cantidad=Desarrollo.objects.get(infante_id=id_infante_autor).CR
                cantidad=cantidad+1
                desarrollo=Desarrollo.objects.filter(infante_id=id_infante_autor).update(CR=cantidad)

            if producto.categoria=='MC' or producto.categoria=='FM' or producto.categoria=='PM' or producto.categoria=='IV' or producto.categoria=='MA' or producto.categoria=='CC' or producto.categoria=='DA' or producto.categoria=='TT':
                cantidad=Desarrollo.objects.get(infante_id=id_infante_autor).SO
                cantidad=cantidad+1
                desarrollo=Desarrollo.objects.filter(infante_id=id_infante_autor).update(SO=cantidad)

            # editamos juguetes para el analisis
            ProductoIntercambio.objects.filter(id_publicacion=solicitud.publicacion.id,foto=jugueteSolicitante.foto,nombre=jugueteSolicitante.nombre,categoria=jugueteSolicitante.categoria).update(estado="deseado",infante=solicitud.publicacion.infante_autor.id)

            # # Creamos Historial
            #
            # historialAnterior=Historial.objects.get(producto=jug_solicitante)
            #
            # historial=Historial()
            # reg=datetime.now()
            # reg_formato=formats.date_format(reg,"DATE_FORMAT")
            # historial.Registro=historialAnterior.Registro
            # historial.Adquisicion=reg_formato
            # n=historialAnterior.Intercambiado+1
            # historial.Intercambiado=n
            # historial.producto=Producto.objects.get(id=jn.id)
            # historial.save()


        #     juguetes_a_borrar.append(jugueteSolicitante.id)
        #
        # for b in juguetes_a_borrar:
        #     p=Producto.objects.get(id=b)
        #     p.delete()

        # juguetePublicador.delete()

            # jugueteSolicitante.galeria=Galeria.objects.get(infante=inf_publicador)
            # jugueteSolicitante.save()
        for solicitud in solicitudes:
            s=Solicitud.objects.get(id=solicitud.id)
            s.Estado="Aceptada"
            s.save()

    response["data"]=r
    return JsonResponse(response)

def cierresPendientes(request):
    response={}
    numero=0

    apiderado_id=request.POST.get('rutApoderado')

    cierresA=Cierre.objects.filter(apoderado_autor_rut=apiderado_id,estado="A concretar")
    cierresB=Cierre.objects.filter(apoderado_destino_rut=apiderado_id,estado="A concretar")

    pendientes=[]

    for cierre in cierresA:
        esta=False
        for i in range(0,len(pendientes)):
            if cierre.producto_a_dar_id == pendientes[i][3]:
                esta=True
                pendientes[i][4].append(cierre.producto_a_recivir_id)
        if esta==False:
            pendientes.append([cierre.dia,cierre.lugar,cierre.hora,cierre.producto_a_dar_id,[cierre.producto_a_recivir_id],cierre.apoderado_autor_rut,cierre.apoderado_destino_rut])

    for cierre in cierresB:
        esta=False
        for i in range(0,len(pendientes)):
            if cierre.producto_a_dar_id == pendientes[i][3]:
                esta=True
                pendientes[i][4].append(cierre.producto_a_recivir_id)
        if esta==False:
            pendientes.append([cierre.dia,cierre.lugar,cierre.hora,cierre.producto_a_dar_id,[cierre.producto_a_recivir_id],cierre.apoderado_autor_rut,cierre.apoderado_destino_rut])

    numero=len(pendientes)

    response["numero"]=numero
    return JsonResponse(response)


def intercambios_a_concretar(request):
    data={}
    data['apoderado']=apoderado=Apoderado.objects.get(usuario_id=request.session.get('id'))

    numero=0

    cierresA=Cierre.objects.filter(apoderado_autor_rut=apoderado.rut).order_by('-id')
    cierresB=Cierre.objects.filter(apoderado_destino_rut=apoderado.rut).order_by('-id')

    pendientes=[]



    for cierre in cierresA:
        esta=False
        jugEsta=False

        if cierre.estado=="A concretar":
            for i in range(0,len(pendientes)):
                if cierre.producto_a_dar_id == pendientes[i][3]:
                    esta=True
                    pendientes[i][4].append(cierre.producto_a_recivir_id)
                    pendientes[i][8].append(cierre.id)
            if esta==False:
                pendientes.append([cierre.dia,cierre.lugar,cierre.hora,cierre.producto_a_dar_id,[cierre.producto_a_recivir_id],cierre.apoderado_autor_rut,cierre.apoderado_destino_rut,cierre.unaSolicitud_id,[cierre.id],cierre.estado])

    pendientesD=[]

    for cierre in cierresB:
        agregar=True
        if cierre.estado=="A concretar":
            for cierrea in cierresA:
                if cierre.id==cierrea.id:
                    agregar=False

            if agregar==True:
                esta=False
                jugEsta=False

                for i in range(0,len(pendientesD)):
                    if cierre.producto_a_dar_id == pendientesD[i][3]:
                        esta=True
                        pendientesD[i][4].append(cierre.producto_a_recivir_id)
                        pendientesD[i][8].append(cierre.id)
                if esta==False:
                    pendientesD.append([cierre.dia,cierre.lugar,cierre.hora,cierre.producto_a_dar_id,[cierre.producto_a_recivir_id],cierre.apoderado_autor_rut,cierre.apoderado_destino_rut,cierre.unaSolicitud_id,[cierre.id],cierre.estado])


    numero=len(pendientes)+len(pendientesD)

    data['pendientesAutor']=pendientes
    data['pendientesDestinatario']=pendientesD
    data['numero']=numero

    template_name='intercambio/concretar.html'
    return render(request,template_name,data)

def juguetesEnintercambios_a_concretar(request):
    response={}
    apoderado=Apoderado.objects.get(usuario_id=request.session.get('id'))
    juguetes=[]
    juguetesUnico=[]

    numero=0

    cierresA=Cierre.objects.filter(apoderado_autor_rut=apoderado.rut,estado="A concretar")
    cierresB=Cierre.objects.filter(apoderado_destino_rut=apoderado.rut,estado="A concretar")

    for cierre in cierresA:
        ju=ProductoIntercambio.objects.get(id=cierre.producto_a_recivir_id)
        juguetes.append([ju.id,ju.nombre,ju.foto.url,ju.categoria])
        jugEsta=False

        for i in range(0,len(juguetesUnico)):
            if cierre.producto_a_dar_id==juguetesUnico[i][0]:
                jugEsta=True

        if jugEsta==False:
            ju=ProductoIntercambio.objects.get(id=cierre.producto_a_dar_id)
            juguetesUnico.append([ju.id,ju.nombre,ju.foto.url,ju.categoria])

    for cierre in cierresB:
        agregar=True
        for cierrea in cierresA:
            if cierre.id==cierrea.id:
                agregar=False

        if agregar==True:
            ju=ProductoIntercambio.objects.get(id=cierre.producto_a_recivir_id)
            juguetes.append([ju.id,ju.nombre,ju.foto.url,ju.categoria])
            jugEsta=False

            for i in range(0,len(juguetesUnico)):
                if cierre.producto_a_dar_id==juguetesUnico[i][0]:
                    jugEsta=True

            if jugEsta==False:
                ju=ProductoIntercambio.objects.get(id=cierre.producto_a_dar_id)
                juguetesUnico.append([ju.id,ju.nombre,ju.foto.url,ju.categoria])

    response['juguetes']=juguetes
    response['juguetesUnico']=juguetesUnico

    return JsonResponse(response)

# first()

def respuestaRealizado(request):
    response={}
    data=[]

    id_unaSolicitud=request.POST.get('unaSolicitudId')
    r=request.POST.get('respues')
    cierres_id=request.POST.getlist('cierreid[]')

    # print(id_unaSolicitud)

    try:
        Existe=ejemplar=Solicitud.objects.get(id=id_unaSolicitud)
    except Solicitud.DoesNotExist:
        Existe=None

    if Existe!=None:
        solicitudes=Solicitud.objects.filter(apoderado_autor_id=ejemplar.apoderado_autor.rut,publicacion_id=ejemplar.publicacion.id)
    else:
        solicitudes=None

    if r=='0':
        if solicitudes!=None:
            for solicitud in solicitudes:
                s=Solicitud.objects.get(id=solicitud.id)
                s.delete()

        for cierre in cierres_id:
            c=Cierre.objects.get(id=cierre)
            c.estado="No realizado"
            c.save()

    else:
        for cierre in cierres_id:
            c=Cierre.objects.get(id=cierre)
            c.estado="Realizado"
            c.save()

        if solicitudes!=None:
            for solicitud in solicitudes:
                inf_publicador=solicitud.publicacion.infante_autor
                jug_publicador=solicitud.publicacion.producto

                inf_solicitante=solicitud.publicacion_deseada.infante_autor
                jug_solicitante=solicitud.publicacion_deseada.producto_a_dar

                break

            # Intercambiamos Juguetes
            # juguete publicador -> solicitante:
            juguetePublicador=Producto.objects.get(id=jug_publicador.id,galeria=Galeria.objects.get(infante=inf_publicador))
            # print("inf Publicador: ",inf_publicador.nombre," juguete: ",jug_publicador.nombre)

            jugueteNuevo=Producto()
            jugueteNuevo.foto=jug_publicador.foto
            jugueteNuevo.nombre=jug_publicador.nombre
            jugueteNuevo.categoria=jug_publicador.categoria
            jugueteNuevo.estado=jug_publicador.estado
            jugueteNuevo.marca=jug_publicador.marca
            jugueteNuevo.modelo=jug_publicador.modelo
            jugueteNuevo.serie=jug_publicador.serie
            jugueteNuevo.anno=jug_publicador.anno
            jugueteNuevo.tamano=jug_publicador.tamano
            jugueteNuevo.material=jug_publicador.material
            jugueteNuevo.Disponible_a_intercambio=False
            jugueteNuevo.galeria=jug_solicitante.galeria

            jugueteNuevo.save()

            jn=Producto.objects.get(id=jugueteNuevo.id)
            jn.generate_qrcode()
            filename = '/codigoQR/qrcode-%s.png' % jugueteNuevo.id
            jn.QR = filename
            jn.save()



            # Creamos Historial
            historialAnterior=Historial.objects.get(producto=jug_publicador)

            historial=Historial()
            reg=datetime.now()
            reg_formato=formats.date_format(reg,"DATE_FORMAT")
            historial.Registro=historialAnterior.Registro
            historial.Adquisicion=reg_formato
            n=historialAnterior.Intercambiado+1
            historial.Intercambiado=n
            historial.producto=Producto.objects.get(id=jn.id)
            historial.save()



            # juguetePublicador.galeria=Galeria.objects.get(infante=inf_solicitante)
            # juguetePublicador.save()

            # print("inf solicitante: ",inf_solicitante.nombre," juguetes:")
            juguetes_a_borrar=[]
            for solicitud in solicitudes:

                jug_solicitante=solicitud.publicacion_deseada.producto_a_dar
                jugueteSolicitante=Producto.objects.get(id=jug_solicitante.id,galeria=Galeria.objects.get(infante=inf_solicitante))
                # print(jug_solicitante.nombre)

                jugueteNuevo=Producto()
                jugueteNuevo.foto=jug_solicitante.foto
                jugueteNuevo.nombre=jug_solicitante.nombre
                jugueteNuevo.categoria=jug_solicitante.categoria
                jugueteNuevo.Disponible_a_intercambio=False
                jugueteNuevo.galeria=jug_publicador.galeria

                jugueteNuevo.save()

                jn=Producto.objects.get(id=jugueteNuevo.id)
                jn.generate_qrcode()
                filename = '/codigoQR/qrcode-%s.png' % jugueteNuevo.id
                jn.QR = filename
                jn.save()


                # Creamos Historial

                historialAnterior=Historial.objects.get(producto=jug_solicitante)

                historial=Historial()
                reg=datetime.now()
                reg_formato=formats.date_format(reg,"DATE_FORMAT")
                historial.Registro=historialAnterior.Registro
                historial.Adquisicion=reg_formato
                n=historialAnterior.Intercambiado+1
                historial.Intercambiado=n
                historial.producto=Producto.objects.get(id=jn.id)
                historial.save()


                juguetes_a_borrar.append(jugueteSolicitante.id)

            for b in juguetes_a_borrar:
                p=Producto.objects.get(id=b)
                p.delete()

            juguetePublicador.delete()

            # jugueteSolicitante.galeria=Galeria.objects.get(infante=inf_publicador)
            # jugueteSolicitante.save()

    response["data"]=r
    return JsonResponse(response)

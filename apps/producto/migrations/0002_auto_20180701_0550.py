# Generated by Django 2.0.4 on 2018-07-01 05:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('producto', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='desarrollo',
            field=models.CharField(blank=True, choices=[('AF', 'Afectividad'), ('MO', 'Motricidad'), ('IN', 'Inteligencia'), ('CR', 'Creatividad'), ('SO', 'Sociabilidad')], default='AF', max_length=2, null=True),
        ),
    ]

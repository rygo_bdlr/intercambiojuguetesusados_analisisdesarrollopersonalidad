from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy
from django.urls import reverse
from django.contrib import messages

from datetime import datetime
from django.utils import formats

from apps.producto.forms import ProductoForm
from apps.producto.models import Producto, Historial
from apps.usuarios.models import Usuario, Infante, Apoderado
from apps.galeria.models import Galeria
from apps.intercambio.models import Publicacion

import qrcode

# Create your views here.
def CrearProducto(request, id_galeria):
    template_name='productos/registro_producto.html'

    usuario=Usuario.objects.get(id=request.session.get('id'))
    noPuedeVer=-1
    if usuario.tipo=="AP":
        if Apoderado.objects.get(usuario_id=request.session.get('id')).rut != Infante.objects.get(id=Galeria.objects.get(id=id_galeria).infante_id).apoderado_id:
            noPuedeVer=0
    elif usuario.tipo=="IN":
        if Infante.objects.get(usuario_id=request.session.get('id')).id != Galeria.objects.get(id=id_galeria).infante_id :
            noPuedeVer=1

    if noPuedeVer==-1:
        if request.method=='POST':
            form=ProductoForm(request.POST, request.FILES)
            if form.is_valid():
                # Guardamos los datos aporados por el usuario
                producto=form.save()
                # Guardamos los datos asignados por el sistema
                Producto.objects.filter(id=producto.id).update(Disponible_a_intercambio=False, galeria=id_galeria)
                # Guardamos la ruta del codigo qr crreado
                prod = Producto.objects.get(id=producto.id)
                prod.generate_qrcode()
                filename = '/codigoQR/qrcode-%s.png' % producto.id
                prod.QR = filename
                prod.save()

                # Creamos Historial
                historial=Historial()
                reg=datetime.now()
                reg_formato=formats.date_format(reg,"DATE_FORMAT")
                historial.Registro=reg_formato
                historial.Adquisicion=reg_formato
                historial.Intercambiado=0
                historial.producto=Producto.objects.get(id=producto.id)
                historial.save()

                id_infante=Galeria.objects.get(id=id_galeria).infante.id

                if usuario.tipo=="AP":
                    return HttpResponseRedirect(reverse('galeria_ap',kwargs={'id_infante':id_infante}))
                elif usuario.tipo=="IN":
                    return HttpResponseRedirect(reverse('galeria_in',kwargs={'id_infante':id_infante}))
        else:
            form=ProductoForm()

        return render(request,template_name,{'form':form})
    elif noPuedeVer==0:
        return HttpResponseRedirect(reverse('inicio_ap'))
    else:
        return HttpResponseRedirect(reverse('intercambio_in'))

def EditarProducto(request, id_producto, id_galeria):
    template_name='productos/editar_producto.html'
    producto= Producto.objects.get(id=id_producto)
    usuario=Usuario.objects.get(id=request.session.get('id'))

    noPuedeVer=-1
    if usuario.tipo=="AP":
        if Apoderado.objects.get(usuario_id=request.session.get('id')).rut != Infante.objects.get(id=Galeria.objects.get(id=id_galeria).infante_id).apoderado_id:
            noPuedeVer=0
    elif usuario.tipo=="IN":
        if Infante.objects.get(usuario_id=request.session.get('id')).id != Galeria.objects.get(id=id_galeria).infante_id :
            noPuedeVer=1

    if noPuedeVer==-1:

        if request.method=='GET':
            form=ProductoForm(instance=producto)
        else:
            form = ProductoForm(request.POST, request.FILES, instance=producto)
            if form.is_valid():
                form.save()

                if usuario.tipo=="AP":
                    # print(id_galeria)
                    return HttpResponseRedirect(reverse('verProducto_ap',kwargs={'id_producto':id_producto,'id_galeria':id_galeria}))
                elif usuario.tipo=="IN":
                    return HttpResponseRedirect(reverse('verProducto_in',kwargs={'id_producto':id_producto,'id_galeria':id_galeria}))
                else:
                    return HttpResponseRedirect(reverse('verProducto_ap',kwargs={'id_producto':id_producto,'id_galeria':id_galeria}))

        return render(request, template_name,{'form':form,'producto':producto,'id_galeria':id_galeria, 'usuario':usuario})

    elif noPuedeVer==0:
        return HttpResponseRedirect(reverse('inicio_ap'))
    else:
        return HttpResponseRedirect(reverse('intercambio_in'))

def verProductoAP(request, id_producto, id_galeria):

    if Apoderado.objects.get(usuario_id=request.session.get('id')).rut == Infante.objects.get(id=Galeria.objects.get(id=id_galeria).infante_id).apoderado_id:

        data={}
        data['producto']=Producto.objects.get(id=id_producto)
        data['galeria']=id_galeria

        data['apoderado']=Apoderado.objects.get(usuario_id=request.session.get('id'))
        data['infantes']=Infante.objects.filter(apoderado_id=Apoderado.objects.get(usuario_id=request.session.get('id')).rut)
        data['historial']=Historial.objects.get(producto=id_producto)

        template_name='productos/ver_producto_ap.html'

        return render(request,template_name,data)
    else:
        return HttpResponseRedirect(reverse('inicio_ap'))


def verProductoIN(request, id_producto, id_galeria):

    if Infante.objects.get(usuario_id=request.session.get('id')).id == Galeria.objects.get(id=id_galeria).infante_id :

        data={}
        data['producto']=Producto.objects.get(id=id_producto)
        data['galeria']=id_galeria

        data['infante']=Infante.objects.get(usuario_id=request.session.get('id'))
        data['historial']=Historial.objects.get(producto=id_producto)

        try:
            data['publicacion']=Publicacion.objects.get(producto=id_producto)
        except Publicacion.DoesNotExist:
            data['publicacion']='None'

        template_name='productos/ver_producto_in.html'

        return render(request,template_name,data)

    else:
        return HttpResponseRedirect(reverse('intercambio_in'))

def verProducto(request, id_producto, id_galeria):
    data={}
    data['producto']=Producto.objects.get(id=id_producto)
    data['galeria']=id_galeria
    data['historial']=Historial.objects.get(producto=id_producto)

    template_name='productos/ver_producto.html'

    return render(request,template_name,data)

from django.forms import ModelForm
from django import forms
from apps.producto.models import Producto

class ProductoForm(forms.ModelForm):
    class Meta:
        model= Producto

        fields=[
            'foto',
            'nombre',
            'categoria',
            'estado',
            'marca',
            'modelo',
            'serie',
            'anno',
            'tamano',
            'material',
        ]
        labels={
            'foto':'Foto:',
            'nombre':'Nombre:',
            'categoria':'Categoria:',
            'estado':'Estado (Opcional)',
            'marca':'Marca (Opcional)',
            'modelo':'Modelo (Opcional)',
            'serie':'Numero de Serie (Opcional)',
            'anno':'Año (Opcional)',
            'tamano':'Tamaño (Opcional)',
            'material':'Material (Opcional)',
        }
        widgets={
            'nombre':forms.TextInput(attrs={'class':'form-control'}),
            'categoria':forms.Select(attrs={'class':'form-control'}),
            'estado':forms.TextInput(attrs={'class':'form-control'}),
            'marca':forms.TextInput(attrs={'class':'form-control'}),
            'modelo':forms.TextInput(attrs={'class':'form-control'}),
            'serie':forms.TextInput(attrs={'class':'form-control'}),
            'anno':forms.TextInput(attrs={'class':'form-control'}),
            'tamano':forms.TextInput(attrs={'class':'form-control'}),
            'material':forms.TextInput(attrs={'class':'form-control'}),
        }

    def save(self,commit=True):
        producto=super(ProductoForm,self).save(commit=False)
        if commit:
            producto.save()
        return producto

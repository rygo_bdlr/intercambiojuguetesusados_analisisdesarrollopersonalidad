import qrcode
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from django.http import HttpResponseRedirect
from django.urls import reverse

from django.db import models
from apps.usuarios.models import Infante
from apps.producto.Choices import Categoria, Desarrollo
from apps.galeria.models import Galeria

from django.core.files import File
import os



# Create your models here.
class ProductoIntercambio(models.Model):
    #atributos
    foto= models.ImageField(upload_to='juguetes')
    nombre=models.CharField(max_length=20)
    categoria=models.CharField(max_length=2, choices= Categoria, default='DV')
    estado=models.CharField(max_length=20, default='deseado')
    estado_d=models.CharField(max_length=250, blank=True,null=True)
    marca=models.CharField(max_length=250, blank=True,null=True)
    modelo=models.CharField(max_length=250, blank=True,null=True)
    serie=models.CharField(max_length=250, blank=True,null=True)
    anno=models.CharField(max_length=250, blank=True,null=True)
    tamano=models.CharField(max_length=250, blank=True,null=True)
    material=models.CharField(max_length=250, blank=True,null=True)

    #LLaves foraneas
    infante = models.PositiveIntegerField(default=0) #infante que se queda con juguete
    id_publicacion=models.PositiveIntegerField(default=0)

class Producto(models.Model):
    #atributos
    foto= models.ImageField(upload_to='juguetes', max_length=500)
    nombre=models.CharField(max_length=20)
    categoria=models.CharField(max_length=2, choices= Categoria, default='DV')
    Disponible_a_intercambio=models.BooleanField(default=False)

    QR = models.ImageField(upload_to='codigoQR', blank=True, null=True)

    estado=models.CharField(max_length=250, blank=True,null=True)
    marca=models.CharField(max_length=250, blank=True,null=True)
    modelo=models.CharField(max_length=250, blank=True,null=True)
    serie=models.CharField(max_length=250, blank=True,null=True)
    anno=models.CharField(max_length=250, blank=True,null=True)
    tamano=models.CharField(max_length=250, blank=True,null=True)
    material=models.CharField(max_length=250, blank=True,null=True)



    #LLaves foraneas
    galeria = models.ForeignKey(Galeria, on_delete=models.CASCADE, blank=True,null=True)


    def save(self,**kwargs):
        super(Producto, self).save()
        # self.generate_qrcode()

    def generate_qrcode(self):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        from django.conf import settings
        link= settings.ALLOWED_HOSTS[0] +":"+ settings.PORT + '/producto/verProducto/%s/%s' % (self.id, self.galeria.id)
        qr.add_data(link)
        qr.make(fit=True)
        filename = '/codigoQR/qrcode-%s.png' % self.id

        img = qr.make_image()

        img.save(settings.MEDIA_ROOT + filename)


    def __str__(self):
        return self.nombre

class Historial(models.Model):
    producto = models.OneToOneField(Producto, on_delete=models.CASCADE, blank=True,null=True)
    Registro = models.CharField(max_length=40)
    Adquisicion = models.CharField(max_length=40)
    Intercambiado = models.PositiveIntegerField()

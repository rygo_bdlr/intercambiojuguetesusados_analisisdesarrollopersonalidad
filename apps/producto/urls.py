from django.urls import path
from apps.producto import views

urlpatterns = [
    path('producto_reg/<int:id_galeria>', views.CrearProducto, name="producto_reg"),
    path('verProducto_ap/<int:id_producto>/<int:id_galeria>', views.verProductoAP, name="verProducto_ap"),
    path('verProducto_in/<int:id_producto>/<int:id_galeria>', views.verProductoIN, name="verProducto_in"),
    path('verProducto/<int:id_producto>/<int:id_galeria>', views.verProducto, name="verProducto"),
    path('editarProducto/<int:id_producto>/<int:id_galeria>', views.EditarProducto, name="editarProducto"),
]

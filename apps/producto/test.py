from django.test import TestCase
from django.test import Client
import unittest

from apps.usuarios.models import Usuario, Apoderado, Infante, Entidad
from apps.intercambio.models import Publicacion, DatosPublicacion, PublicacionDeseada, Solicitud, Cierre
from apps.usuarios.models import Usuario, Apoderado, Entidad, Infante
from apps.producto.models import Producto,ProductoIntercambio, Historial
from apps.galeria.models import Galeria
from apps.analisis.models import Desarrollo, Preferencias

from django.urls import reverse

import json

from django.core.files.uploadedfile import SimpleUploadedFile


class ProductoTest(TestCase):

    @classmethod
    def setUp(self):
        self.usuario_ap= Usuario.objects.create(password="password", username="user_ap", tipo="AP")
        self.usuario_in= Usuario.objects.create(password="password", username="user_in", tipo="IN")

        self.apoderado= Apoderado.objects.create(rut="1",nombre="apoderado",apellido="apellido",edad=1,movil=1,email="a@gmail.com",usuario=self.usuario_ap,foto="foto")
        self.infante= Infante.objects.create(nombre="niño",edad=1,apoderado=self.apoderado,usuario=self.usuario_in,foto="foto")
        self.galeria=Galeria.objects.create(limiteProductos=100,infante=self.infante)

        self.producto=Producto.objects.create(foto="foto",nombre="juguete",categoria="DV",Disponible_a_intercambio=False,QR="qr",galeria=self.galeria)
        self.historial=Historial.objects.create(producto=self.producto,Registro="test",Adquisicion="test",Intercambiado=0)

        self.publicacion=Publicacion.objects.create(Estado="Publicado",producto=self.producto,infante_autor=self.infante)
        self.datosPublicacion=DatosPublicacion.objects.create(dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",apoderado_autor=self.apoderado,publicacion=self.publicacion)

    @classmethod
    def tearDown(self):
        Usuario.objects.all().delete()
        Apoderado.objects.all().delete()
        Infante.objects.all().delete()
        Galeria.objects.all().delete()
        Producto.objects.all().delete()
        Historial.objects.all().delete()
        Publicacion.objects.all().delete()
        DatosPublicacion.objects.all().delete()

    def testUnitarioProducto(self):
        self.assertEqual(self.producto.__str__(),self.producto.nombre)

    def testCrearProducto(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}),
            {"foto": foto, "nombre": 'juguete', "categoria": 'DV'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testEditarProducto(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('editarProducto',kwargs={"id_producto": self.producto.pk,"id_galeria": self.galeria.pk}),
            {"foto": foto, "nombre": 'juguete', "categoria": 'DV'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testVerProductoAP(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        response = self.client.get(reverse('verProducto_ap',kwargs={"id_producto": self.producto.pk,"id_galeria": self.galeria.pk}))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'productos/ver_producto_ap.html')

    def testVerProductoIN(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session.save()

        response = self.client.get(reverse('verProducto_in',kwargs={"id_producto": self.producto.pk,"id_galeria": self.galeria.pk}))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'productos/ver_producto_in.html')

    def testVerProducto(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session.save()

        response = self.client.get(reverse('verProducto',kwargs={"id_producto": self.producto.pk,"id_galeria": self.galeria.pk}))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'productos/ver_producto.html')

Categoria=(
    ('DV','Deportivos y vehículos'), #Motricidad
    ('LL','Libros'), #Inteligencia
    ('RM','Robot y mecánicos'), #Creatividad
    ('MC','Muñecas y accesorios'), #Afectividad, Creatividad, Sociabilidad
    ('FM','Figuras y muñecos de acción'), #Creatividad, Sociabilidad
    ('PM','Peluches y muñecos de trapo'), #Afectividad, Creatividad, Sociabilidad
    ('AV','Autos y vehículos en miniatura'), #Creatividad
    ('TA','Trenes y accesorios'), #Creatividad
    ('AA','Autopista y accesorios'), #Creatividad
    ('HH','Herramientas'), #Motricidad, Inteligencia, Creatividad
    ('MM','Materiales'), #Motricidad, Inteligencia, Creatividad
    ('CA','Construcción, acoplamientos y "kits"'), #Motricidad, Inteligencia, Creatividad
    ('IV','Imitación de la vida'), #Afectividad, Creatividad, Sociabilidad
    ('MU','Musicales'), #Motricidad
    ('AV','Audio-visual'), #Creatividad
    ('CT','Ciencia y tecnología'), #Inteligencia, Creatividad
    ('PR','Puzzles y rompecabezas'), #Motricidad, Inteligencia
    ('MA','Juego de mesa y azar'), #Inteligencia, Sociabilidad
    ('VO','Videoconsola y ordenador'), #Inteligencia
    ('CC','Comunicación'), #Afectividad, Sociabilidad
    ('PH','Puntería y habilidad'), #Motricidad
    ('AE','Arrastre y empuje'), #Motricidad, Inteligencia
    ('OV','Voladores'), #Creatividad
    ('DA','Disfraces y accesorios'), #Creatividad, Sociabilidad
    ('TT','Títeres y teatro'), #Creatividad, Sociabilidad
)

Desarrollo=(
    ('AF','Afectividad'),
    ('MO','Motricidad'),
    ('IN','Inteligencia'),
    ('CR','Creatividad'),
    ('SO','Sociabilidad'),
)

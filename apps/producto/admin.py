from django.contrib import admin
from apps.producto.models import Producto, Historial, ProductoIntercambio

# Register your models here.
@admin.register(Producto)
class AdminProducto(admin.ModelAdmin):
    list_display=('nombre',)

@admin.register(Historial)
class AdminHistorial(admin.ModelAdmin):
    list_display=('producto',)

@admin.register(ProductoIntercambio)
class AdminProductoIntercambio(admin.ModelAdmin):
    list_display=('nombre',)

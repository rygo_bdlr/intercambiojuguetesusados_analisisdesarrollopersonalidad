from django.urls import path
from apps.analisis import views

urlpatterns = [
    path('VerAnalisis_in', views.VerAnalisis_in, name="VerAnalisis_in"),
    path('VerAnalisis_ap/<int:infante_id>', views.VerAnalisis_ap, name="VerAnalisis_ap"),
    path('Sectores', views.Sectores, name="Sectores"),
    path('VerInfoSector', views.VerInfoSector, name="VerInfoSector"),
]

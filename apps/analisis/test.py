from django.test import TestCase
from django.test import Client
import unittest

from apps.usuarios.models import Usuario, Apoderado, Infante, Entidad
from apps.intercambio.models import Publicacion, DatosPublicacion, PublicacionDeseada, Solicitud, Cierre
from apps.usuarios.models import Usuario, Apoderado, Entidad, Infante
from apps.producto.models import Producto,ProductoIntercambio, Historial
from apps.galeria.models import Galeria
from apps.analisis.models import Desarrollo, Preferencias

from django.urls import reverse

import json


class AnalisisTest(TestCase):

    @classmethod
    def setUp(self):

        self.usuario_ap= Usuario.objects.create(password="password", username="user_ap", tipo="AP")
        self.usuario_ap2= Usuario.objects.create(password="password", username="user_ap2", tipo="AP")
        self.usuario_in= Usuario.objects.create(password="password", username="user_in", tipo="IN")
        self.usuario_in2= Usuario.objects.create(password="password", username="user_in2", tipo="IN")
        self.usuario_en= Usuario.objects.create(password="password", username="user_en", tipo="EN")

        self.apoderado= Apoderado.objects.create(rut="1",nombre="apoderado",apellido="apellido",edad=1,movil=1,email="a@gmail.com",usuario=self.usuario_ap,foto="foto")
        self.apoderado2= Apoderado.objects.create(rut="2",nombre="apoderado2",apellido="apellido2",edad=1,movil=1,email="a2@gmail.com",usuario=self.usuario_ap2,foto="foto")
        self.infante= Infante.objects.create(nombre="niño",edad=1,apoderado=self.apoderado,usuario=self.usuario_in,foto="foto")
        self.infante2= Infante.objects.create(nombre="niño2",edad=1,apoderado=self.apoderado2,usuario=self.usuario_in2,foto="foto")
        self.galeria=Galeria.objects.create(limiteProductos=100,infante=self.infante)
        self.galeria2=Galeria.objects.create(limiteProductos=100,infante=self.infante2)
        self.entidad= Entidad.objects.create(nombre="entidad",email="e@gmail.com",pagina="pagina",usuario=self.usuario_en,foto="foto")

        self.desarrollo= Desarrollo.objects.create(AF=1,infante=self.infante,CR=1,IN=1,MO=1,SO=1)
        self.preferencias= Preferencias.objects.create(infante=self.infante,DV=1,LL=1,RM=1,MC=1,FM=1,PM=1,AV=1,TA=1,AA=1,HH=1,MM=1,CA=1,IV=1,MU=1,CT=1,PR=1,MA=1,VO=1,CC=1,PH=1,AE=1,OV=1,DA=1,TT=1)

        self.client= Client()



        ProductoIntercambio.objects.create(foto="foto",nombre="juguetePublicacion",categoria="DV",estado="deseado",id_publicacion=1,infante=self.infante2.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteDV",categoria="DV",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteLL",categoria="LL",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteRM",categoria="RM",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteMC",categoria="MC",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteFM",categoria="FM",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="juguetePM",categoria="PM",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteAV",categoria="AV",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteTA",categoria="TA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteAA",categoria="AA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteHH",categoria="HH",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteMM",categoria="MM",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteCA",categoria="CA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteIV",categoria="IV",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteMU",categoria="MU",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteAV",categoria="AV",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteCT",categoria="CT",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="juguetePR",categoria="PR",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteMA",categoria="MA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteVO",categoria="VO",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteCC",categoria="CC",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="juguetePH",categoria="PH",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteAE",categoria="AE",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteOV",categoria="OV",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteDA",categoria="DA",estado="deseado",id_publicacion=1,infante=self.infante.id)
        ProductoIntercambio.objects.create(foto="foto",nombre="jugueteTT",categoria="TT",estado="deseado",id_publicacion=1,infante=self.infante.id)

        self.productoPublicacion=Producto.objects.create(foto="foto",nombre="juguetePublicacion",categoria="DV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria)
        self.producto_a_darDV=Producto.objects.create(foto="foto",nombre="jugueteDV",categoria="DV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darLL=Producto.objects.create(foto="foto",nombre="jugueteLL",categoria="LL",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darRM=Producto.objects.create(foto="foto",nombre="jugueteRM",categoria="RM",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darMC=Producto.objects.create(foto="foto",nombre="jugueteMC",categoria="MC",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darFM=Producto.objects.create(foto="foto",nombre="jugueteFM",categoria="FM",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darPM=Producto.objects.create(foto="foto",nombre="juguetePM",categoria="PM",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darAV=Producto.objects.create(foto="foto",nombre="jugueteAV",categoria="AV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darTA=Producto.objects.create(foto="foto",nombre="jugueteTA",categoria="TA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darAA=Producto.objects.create(foto="foto",nombre="jugueteAA",categoria="AA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darHH=Producto.objects.create(foto="foto",nombre="jugueteHH",categoria="HH",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darMM=Producto.objects.create(foto="foto",nombre="jugueteMM",categoria="MM",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darCA=Producto.objects.create(foto="foto",nombre="jugueteCA",categoria="CA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darIV=Producto.objects.create(foto="foto",nombre="jugueteIV",categoria="IV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darMU=Producto.objects.create(foto="foto",nombre="jugueteMU",categoria="MU",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darAV=Producto.objects.create(foto="foto",nombre="jugueteAV",categoria="AV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darCT=Producto.objects.create(foto="foto",nombre="jugueteCT",categoria="CT",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darPR=Producto.objects.create(foto="foto",nombre="juguetePR",categoria="PR",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darMA=Producto.objects.create(foto="foto",nombre="jugueteMA",categoria="MA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darVO=Producto.objects.create(foto="foto",nombre="jugueteVO",categoria="VO",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darCC=Producto.objects.create(foto="foto",nombre="jugueteCC",categoria="CC",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darPH=Producto.objects.create(foto="foto",nombre="juguetePH",categoria="PH",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darAE=Producto.objects.create(foto="foto",nombre="jugueteAE",categoria="AE",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darOV=Producto.objects.create(foto="foto",nombre="jugueteOV",categoria="OV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darDA=Producto.objects.create(foto="foto",nombre="jugueteDA",categoria="DA",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)
        self.producto_a_darTT=Producto.objects.create(foto="foto",nombre="jugueteTT",categoria="TT",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria2)

        self.publicacion=Publicacion.objects.create(Estado="Publicado",producto=self.productoPublicacion,infante_autor=self.infante)

        self.datosPublicacion=DatosPublicacion.objects.create(dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",apoderado_autor=self.apoderado,publicacion=self.publicacion)

        self.publicacionDeseadaDV=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darDV)
        self.publicacionDeseadaLL=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darLL)
        self.publicacionDeseadaRM=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darRM)
        self.publicacionDeseadaMC=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMC)
        self.publicacionDeseadaFM=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darFM)
        self.publicacionDeseadaPM=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darPM)
        self.publicacionDeseadaAV=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAV)
        self.publicacionDeseadaTA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darTA)
        self.publicacionDeseadaAA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAA)
        self.publicacionDeseadaHH=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darHH)
        self.publicacionDeseadaMM=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMM)
        self.publicacionDeseadaCA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darCA)
        self.publicacionDeseadaIV=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darIV)
        self.publicacionDeseadaMU=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMU)
        self.publicacionDeseadaAV=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAV)
        self.publicacionDeseadaCT=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darCT)
        self.publicacionDeseadaPR=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darPR)
        self.publicacionDeseadaMA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darMA)
        self.publicacionDeseadaVO=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darVO)
        self.publicacionDeseadaCC=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darCC)
        self.publicacionDeseadaPH=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darPH)
        self.publicacionDeseadaAE=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darAE)
        self.publicacionDeseadaOV=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darOV)
        self.publicacionDeseadaDA=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darDA)
        self.publicacionDeseadaTT=PublicacionDeseada.objects.create(infante_autor=self.infante2,publicacion=self.publicacion,producto_a_dar=self.producto_a_darTT)

        self.solicitudDV=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaDV)
        self.solicitudLL=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaLL)
        self.solicitudRM=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaRM)
        self.solicitudMC=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMC)
        self.solicitudFM=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaFM)
        self.solicitudPM=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaPM)
        self.solicitudAV=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAV)
        self.solicitudTA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaTA)
        self.solicitudAA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAA)
        self.solicitudHH=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaHH)
        self.solicitudMM=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMM)
        self.solicitudCA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaCA)
        self.solicitudIV=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaIV)
        self.solicitudMU=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMU)
        self.solicitudAV=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAV)
        self.solicitudCT=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaCT)
        self.solicitudPR=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaPR)
        self.solicitudMA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaMA)
        self.solicitudVO=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaVO)
        self.solicitudCC=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaCC)
        self.solicitudPH=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaPH)
        self.solicitudAE=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaAE)
        self.solicitudOV=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaOV)
        self.solicitudDA=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaDA)
        self.solicitudTT=Solicitud.objects.create(Estado="En espera",publicacion=self.publicacion,apoderado_autor=self.apoderado2,datod_publicacion=self.datosPublicacion,publicacion_deseada=self.publicacionDeseadaTT)

        self.cierreDV=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darDV.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreLL=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darLL.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreRM=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darRM.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreMC=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darMC.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreFM=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darFM.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierrePM=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darPM.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreAV=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darAV.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreTA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darTA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreAA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darAA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreHH=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darHH.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreMM=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darMM.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreCA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darCA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreIV=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darIV.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreMU=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darMU.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreAV=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darAV.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreCT=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darCT.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierrePR=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darPR.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreMA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darMA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreVO=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darVO.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreCC=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darCC.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierrePH=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darPH.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreAE=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darAE.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreOV=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darOV.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreDA=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darDA.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")
        self.cierreTT=Cierre.objects.create(producto_a_recivir_id=self.producto_a_darTT.id,producto_a_dar_id=self.productoPublicacion.id,apoderado_autor_rut=self.apoderado.rut,apoderado_destino_rut=self.apoderado2.rut,unaSolicitud_id=self.solicitudDV.id,dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",estado="A concretar")

    @classmethod
    def tearDown(self):
        Usuario.objects.all().delete()
        Apoderado.objects.all().delete()
        Infante.objects.all().delete()
        Entidad.objects.all().delete()
        Desarrollo.objects.all().delete()
        Preferencias.objects.all().delete()
        ProductoIntercambio.objects.all().delete()
        Producto.objects.all().delete()
        Publicacion.objects.all().delete()
        DatosPublicacion.objects.all().delete()
        PublicacionDeseada.objects.all().delete()
        Solicitud.objects.all().delete()
        Cierre.objects.all().delete()

    def testVerAnalisis_in(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session.save()

        response = self.client.get(reverse('VerAnalisis_in'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'analisis/analisis_in.html')

    def testVerAnalisis_ap(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        response = self.client.get(reverse('VerAnalisis_ap', kwargs={"infante_id": self.infante.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'analisis/analisis_ap.html')

    def testSectores(self):
        self.client.force_login(self.usuario_en)
        session = self.client.session
        session['id']=self.usuario_en.id
        session.save()

        response = self.client.get(reverse('Sectores'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'analisis/analisis_en.html')

    def testVerInfoSector(self):
        self.client.force_login(self.usuario_en)
        session = self.client.session
        session['id']=self.usuario_en.id
        session.save()

        array=[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26]

        response = self.client.post(
            reverse('VerInfoSector'),
            {"juguetes_id[]": array}
        )
        self.assertEqual(response.status_code, 200)
        # self.assertIn(array, response.content)

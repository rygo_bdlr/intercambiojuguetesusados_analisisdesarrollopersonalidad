from django.contrib import admin
from apps.analisis.models import Desarrollo, Preferencias

# Register your models here.
@admin.register(Desarrollo)
class AdminDesarrollo(admin.ModelAdmin):
    list_display=('infante',)

@admin.register(Preferencias)
class AdminPreferencias(admin.ModelAdmin):
    list_display=('infante',)

# Generated by Django 2.0.4 on 2018-10-26 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analisis', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='juguetesenpreferencia',
            name='juguete',
        ),
        migrations.RemoveField(
            model_name='juguetesenpreferencia',
            name='preferencia',
        ),
        migrations.RenameField(
            model_name='desarrollo',
            old_name='nivel',
            new_name='AF',
        ),
        migrations.RenameField(
            model_name='preferencias',
            old_name='nivel',
            new_name='AA',
        ),
        migrations.RemoveField(
            model_name='desarrollo',
            name='tipo',
        ),
        migrations.RemoveField(
            model_name='preferencias',
            name='categoria',
        ),
        migrations.AddField(
            model_name='desarrollo',
            name='CR',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='desarrollo',
            name='IN',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='desarrollo',
            name='MO',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='desarrollo',
            name='SO',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='AE',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='AV',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='CA',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='CC',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='CT',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='DA',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='DV',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='FM',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='HH',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='IV',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='LL',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='MA',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='MC',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='MM',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='MU',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='OV',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='PH',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='PM',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='PR',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='RM',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='TA',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='TT',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='preferencias',
            name='VO',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.DeleteModel(
            name='juguetesEnPreferencia',
        ),
    ]

from django.db import models
from apps.usuarios.models import Apoderado, Infante
from apps.producto.models import ProductoIntercambio
from apps.producto.Choices import Categoria, Desarrollo


class Desarrollo(models.Model):
    #atributos
    AF=models.PositiveIntegerField(default=0)
    MO=models.PositiveIntegerField(default=0)
    IN=models.PositiveIntegerField(default=0)
    CR=models.PositiveIntegerField(default=0)
    SO=models.PositiveIntegerField(default=0)

    #LLaves foraneas
    infante= models.OneToOneField(Infante, on_delete=models.CASCADE, blank=True,null=True)


class Preferencias(models.Model):
    #atributos
    DV=models.PositiveIntegerField(default=0)
    LL=models.PositiveIntegerField(default=0)
    RM=models.PositiveIntegerField(default=0)
    MC=models.PositiveIntegerField(default=0)
    FM=models.PositiveIntegerField(default=0)
    PM=models.PositiveIntegerField(default=0)
    AV=models.PositiveIntegerField(default=0)
    TA=models.PositiveIntegerField(default=0)
    AA=models.PositiveIntegerField(default=0)
    HH=models.PositiveIntegerField(default=0)
    MM=models.PositiveIntegerField(default=0)
    CA=models.PositiveIntegerField(default=0)
    IV=models.PositiveIntegerField(default=0)
    MU=models.PositiveIntegerField(default=0)
    AV=models.PositiveIntegerField(default=0)
    CT=models.PositiveIntegerField(default=0)
    PR=models.PositiveIntegerField(default=0)
    MA=models.PositiveIntegerField(default=0)
    VO=models.PositiveIntegerField(default=0)
    CC=models.PositiveIntegerField(default=0)
    PH=models.PositiveIntegerField(default=0)
    AE=models.PositiveIntegerField(default=0)
    OV=models.PositiveIntegerField(default=0)
    DA=models.PositiveIntegerField(default=0)
    TT=models.PositiveIntegerField(default=0)

    #LLaves foraneas
    infante= models.OneToOneField(Infante, on_delete=models.CASCADE, blank=True,null=True)

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.db.models import Q

from apps.intercambio.models import Publicacion, DatosPublicacion, PublicacionDeseada, Solicitud, Cierre
from apps.usuarios.models import Usuario, Apoderado, Entidad, Infante
from apps.producto.models import Producto,ProductoIntercambio, Historial
from apps.galeria.models import Galeria
from apps.analisis.models import Desarrollo, Preferencias

from datetime import datetime
from django.utils import formats

from operator import itemgetter

import json

# Create your views here.

def VerAnalisis_in(request):
    data={}
    data['infante']=Infante.objects.get(usuario_id=request.session.get('id'))
    data['desarrollo']=Desarrollo.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id')))
    preferencias=[]

    preferencias.append(['Deportivos y vehículos',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).DV,[]])
    preferencias.append(['Libros',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).LL,[]])
    preferencias.append(['Robot y mecánicos',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).RM,[]])
    preferencias.append(['Muñecas y accesorios',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).MC,[]])
    preferencias.append(['Figuras y muñecos de acción',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).FM,[]])
    preferencias.append(['Peluches y muñecos de trapo',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).PM,[]])
    preferencias.append(['Autos y vehículos en miniatura',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).AV,[]])
    preferencias.append(['Trenes y accesorios',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).TA,[]])
    preferencias.append(['Autopista y accesorios',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).AA,[]])
    preferencias.append(['Herramientas',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).HH,[]])
    preferencias.append(['Materiales',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).MM,[]])
    preferencias.append(['Construcción, acoplamientos y "kits',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).CA,[]])
    preferencias.append(['Imitación de la vida',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).IV,[]])
    preferencias.append(['Musicales',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).MU,[]])
    preferencias.append(['Audio-visual',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).AV,[]])
    preferencias.append(['Ciencia y tecnología',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).CT,[]])
    preferencias.append(['Puzzles y rompecabezas',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).PR,[]])
    preferencias.append(['Juego de mesa y azar',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).MA,[]])
    preferencias.append(['Videoconsola y ordenador',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).VO,[]])
    preferencias.append(['Comunicación',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).CC,[]])
    preferencias.append(['Puntería y habilidad',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).PH,[]])
    preferencias.append(['Arrastre y empuje',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).AE,[]])
    preferencias.append(['Voladores',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).OV,[]])
    preferencias.append(['Disfraces y accesorios',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).DA,[]])
    preferencias.append(['Títeres y teatro',Preferencias.objects.get(infante=Infante.objects.get(usuario_id=request.session.get('id'))).TT,[]])



    # Preferencia [0]='AA' [1]=n° [2]=[j0,j1,j2,...]
    productos=ProductoIntercambio.objects.filter(infante=Infante.objects.get(usuario_id=request.session.get('id')).id)
    for p in productos:
        if p.estado=='deseado':
            if p.categoria=='DV':
                preferencias[0][2].append([p.nombre,p.foto.url])
            elif p.categoria=='LL':
                preferencias[1][2].append([p.nombre,p.foto.url])
            elif p.categoria=='RM':
                preferencias[2][2].append([p.nombre,p.foto.url])
            elif p.categoria=='MC':
                preferencias[3][2].append([p.nombre,p.foto.url])
            elif p.categoria=='FM':
                preferencias[4][2].append([p.nombre,p.foto.url])
            elif p.categoria=='PM':
                preferencias[5][2].append([p.nombre,p.foto.url])
            elif p.categoria=='AV':
                preferencias[6][2].append([p.nombre,p.foto.url])
            elif p.categoria=='TA':
                preferencias[7][2].append([p.nombre,p.foto.url])
            elif p.categoria=='AA':
                preferencias[8][2].append([p.nombre,p.foto.url])
            elif p.categoria=='HH':
                preferencias[9][2].append([p.nombre,p.foto.url])
            elif p.categoria=='MM':
                preferencias[10][2].append([p.nombre,p.foto.url])
            elif p.categoria=='CA':
                preferencias[11][2].append([p.nombre,p.foto.url])
            elif p.categoria=='IV':
                preferencias[12][2].append([p.nombre,p.foto.url])
            elif p.categoria=='MU':
                preferencias[13][2].append([p.nombre,p.foto.url])
            # elif p.categoria=='AV':
            #     preferencias[14][2].append([p.nombre,p.foto.url])
            elif p.categoria=='CT':
                preferencias[15][2].append([p.nombre,p.foto.url])
            elif p.categoria=='PR':
                preferencias[16][2].append([p.nombre,p.foto.url])
            elif p.categoria=='MA':
                preferencias[17][2].append([p.nombre,p.foto.url])
            elif p.categoria=='VO':
                preferencias[18][2].append([p.nombre,p.foto.url])
            elif p.categoria=='CC':
                preferencias[19][2].append([p.nombre,p.foto.url])
            elif p.categoria=='PH':
                preferencias[20][2].append([p.nombre,p.foto.url])
            elif p.categoria=='AE':
                preferencias[21][2].append([p.nombre,p.foto.url])
            elif p.categoria=='OV':
                preferencias[22][2].append([p.nombre,p.foto.url])
            elif p.categoria=='DA':
                preferencias[23][2].append([p.nombre,p.foto.url])
            elif p.categoria=='TT':
                preferencias[24][2].append([p.nombre,p.foto.url])

    preferencias.sort(reverse=True,key=itemgetter(1))

    data['preferencias']=preferencias

    template_name='analisis/analisis_in.html'
    return render(request,template_name,data)

def VerAnalisis_ap(request,infante_id):
    data={}

    if Apoderado.objects.get(usuario_id=request.session.get('id')).rut == Infante.objects.get(id=infante_id).apoderado_id:

        data['apoderado']=Apoderado.objects.get(usuario_id=request.session.get('id'))
        data['infante']=Infante.objects.get(id=infante_id)
        data['desarrollo']=Desarrollo.objects.get(infante=Infante.objects.get(id=infante_id))
        preferencias=[]

        preferencias.append(['Deportivos y vehículos',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).DV,[]])
        preferencias.append(['Libros',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).LL,[]])
        preferencias.append(['Robot y mecánicos',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).RM,[]])
        preferencias.append(['Muñecas y accesorios',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).MC,[]])
        preferencias.append(['Figuras y muñecos de acción',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).FM,[]])
        preferencias.append(['Peluches y muñecos de trapo',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).PM,[]])
        preferencias.append(['Autos y vehículos en miniatura',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).AV,[]])
        preferencias.append(['Trenes y accesorios',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).TA,[]])
        preferencias.append(['Autopista y accesorios',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).AA,[]])
        preferencias.append(['Herramientas',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).HH,[]])
        preferencias.append(['Materiales',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).MM,[]])
        preferencias.append(['Construcción, acoplamientos y "kits',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).CA,[]])
        preferencias.append(['Imitación de la vida',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).IV,[]])
        preferencias.append(['Musicales',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).MU,[]])
        # preferencias.append(['Audio-visual',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).AV,[]])
        preferencias.append(['Audio-visual',0,[]])
        preferencias.append(['Ciencia y tecnología',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).CT,[]])
        preferencias.append(['Puzzles y rompecabezas',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).PR,[]])
        preferencias.append(['Juego de mesa y azar',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).MA,[]])
        preferencias.append(['Videoconsola y ordenador',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).VO,[]])
        preferencias.append(['Comunicación',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).CC,[]])
        preferencias.append(['Puntería y habilidad',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).PH,[]])
        preferencias.append(['Arrastre y empuje',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).AE,[]])
        preferencias.append(['Voladores',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).OV,[]])
        preferencias.append(['Disfraces y accesorios',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).DA,[]])
        preferencias.append(['Títeres y teatro',Preferencias.objects.get(infante=Infante.objects.get(id=infante_id)).TT,[]])



        # Preferencia [0]='AA' [1]=n° [2]=[j0,j1,j2,...]
        productos=ProductoIntercambio.objects.filter(infante=Infante.objects.get(id=infante_id).id)
        for p in productos:
            if p.estado=='deseado':
                if p.categoria=='DV':
                    preferencias[0][2].append([p.nombre,p.foto.url])
                elif p.categoria=='LL':
                    preferencias[1][2].append([p.nombre,p.foto.url])
                elif p.categoria=='RM':
                    preferencias[2][2].append([p.nombre,p.foto.url])
                elif p.categoria=='MC':
                    preferencias[3][2].append([p.nombre,p.foto.url])
                elif p.categoria=='FM':
                    preferencias[4][2].append([p.nombre,p.foto.url])
                elif p.categoria=='PM':
                    preferencias[5][2].append([p.nombre,p.foto.url])
                elif p.categoria=='AV':
                    preferencias[6][2].append([p.nombre,p.foto.url])
                elif p.categoria=='TA':
                    preferencias[7][2].append([p.nombre,p.foto.url])
                elif p.categoria=='AA':
                    preferencias[8][2].append([p.nombre,p.foto.url])
                elif p.categoria=='HH':
                    preferencias[9][2].append([p.nombre,p.foto.url])
                elif p.categoria=='MM':
                    preferencias[10][2].append([p.nombre,p.foto.url])
                elif p.categoria=='CA':
                    preferencias[11][2].append([p.nombre,p.foto.url])
                elif p.categoria=='IV':
                    preferencias[12][2].append([p.nombre,p.foto.url])
                elif p.categoria=='MU':
                    preferencias[13][2].append([p.nombre,p.foto.url])
                # elif p.categoria=='AV':
                #     preferencias[14][2].append([p.nombre,p.foto.url])
                elif p.categoria=='CT':
                    preferencias[15][2].append([p.nombre,p.foto.url])
                elif p.categoria=='PR':
                    preferencias[16][2].append([p.nombre,p.foto.url])
                elif p.categoria=='MA':
                    preferencias[17][2].append([p.nombre,p.foto.url])
                elif p.categoria=='VO':
                    preferencias[18][2].append([p.nombre,p.foto.url])
                elif p.categoria=='CC':
                    preferencias[19][2].append([p.nombre,p.foto.url])
                elif p.categoria=='PH':
                    preferencias[20][2].append([p.nombre,p.foto.url])
                elif p.categoria=='AE':
                    preferencias[21][2].append([p.nombre,p.foto.url])
                elif p.categoria=='OV':
                    preferencias[22][2].append([p.nombre,p.foto.url])
                elif p.categoria=='DA':
                    preferencias[23][2].append([p.nombre,p.foto.url])
                elif p.categoria=='TT':
                    preferencias[24][2].append([p.nombre,p.foto.url])

        preferencias.sort(reverse=True,key=itemgetter(1))

        data['preferencias']=preferencias

        template_name='analisis/analisis_ap.html'
        return render(request,template_name,data)

    else:
        return HttpResponseRedirect(reverse('inicio_ap'))

def Sectores(request):
    data={}
    data['entidad']=Entidad.objects.get(usuario_id=request.session.get('id'))
    data['bienv']=request.session.get('bienv')
    request.session['bienv']=False

    datos=[] # datos=[[pais,ciudad,comuna,n°inter,[j,j]],[pais,ciudad,comuna,n°inter,[j,j]]]
    cierres=Cierre.objects.all()
    UnCierre=-1
    for cierre in cierres:
        # capturamos pais ciudad y comuna
        lugarTodo=cierre.lugar
        lugar=lugarTodo.split(",")

        pais="Sin especificar"
        ciudad="Sin especificar"
        comuna="Sin especificar"
        if len(lugar)>=1:
            pais=lugar[len(lugar)-1]
            if len(lugar)>=2:
                ciudad=lugar[len(lugar)-2]
                if len(lugar)>=3:
                    comuna=lugar[len(lugar)-3]

        mismo=False
        if cierre.unaSolicitud_id==UnCierre:
            mismo=True

        UnCierre=cierre.unaSolicitud_id
        # buscamos si ya esta registrado en datos un lugar similar
        # datos=[[pais,ciudad,comuna,n°inter,[j,j]],[pais,ciudad,comuna,n°inter,[j,j]]]
        esta=False
        lenD=len(datos)
        for i in range(0,lenD):
            if datos[i][0]==pais and datos[i][1]==ciudad and datos[i][2]==comuna:
                esta=True
                if mismo==False:
                    datos[i][3]=datos[i][3]+1

                JestaA=False
                JestaB=False
                lenJ=len(datos[i][4])
                for j in range(0,lenJ):
                    if cierre.producto_a_dar_id==datos[i][4][j]:
                        JestaA=True
                    if cierre.producto_a_recivir_id==datos[i][4][j]:
                        JestaB=True

                if JestaA==False:
                    datos[i][4].append(cierre.producto_a_dar_id)
                if JestaB==False:
                    datos[i][4].append(cierre.producto_a_recivir_id)

                break

        if esta==False:
            datos.append([pais,ciudad,comuna,1,[cierre.producto_a_dar_id,cierre.producto_a_recivir_id]])


    data['sectores']=datos



    template_name='analisis/analisis_en.html'
    return render(request,template_name,data)

def VerInfoSector(request):
    response={}
    data=[]
    productos=[]
    preferencias=[]

    preferencias.append(['Deportivos y vehículos',0])
    preferencias.append(['Libros',0])
    preferencias.append(['Robot y mecánicos',0])
    preferencias.append(['Muñecas y accesorios',0])
    preferencias.append(['Figuras y muñecos de acción',0])
    preferencias.append(['Peluches y muñecos de trapo',0])
    preferencias.append(['Autos y vehículos en miniatura',0])
    preferencias.append(['Trenes y accesorios',0])
    preferencias.append(['Autopista y accesorios',0])
    preferencias.append(['Herramientas',0])
    preferencias.append(['Materiales',0])
    preferencias.append(['Construcción, acoplamientos y "kits',0])
    preferencias.append(['Imitación de la vida',0])
    preferencias.append(['Musicales',0])
    preferencias.append(['Audio-visual',0])
    preferencias.append(['Ciencia y tecnología',0])
    preferencias.append(['Puzzles y rompecabezas',0])
    preferencias.append(['Juego de mesa y azar',0])
    preferencias.append(['Videoconsola y ordenador',0])
    preferencias.append(['Comunicación',0])
    preferencias.append(['Puntería y habilidad',0])
    preferencias.append(['Arrastre y empuje',0])
    preferencias.append(['Voladores',0])
    preferencias.append(['Disfraces y accesorios',0])
    preferencias.append(['Títeres y teatro',0])

    id_juguetes=request.POST.getlist('juguetes_id[]')

    lenJ=len(id_juguetes)
    for i in range(0,lenJ):
        p=ProductoIntercambio.objects.get(id=id_juguetes[i])
        categoria="---"
        if p.categoria=='DV':
            categoria='Deportivos y vehículos'
            preferencias[0][1]=preferencias[0][1]+1
        elif p.categoria=='LL':
            categoria='Libros'
            preferencias[1][1]=preferencias[1][1]+1
        elif p.categoria=='RM':
            categoria='Robot y mecánicos'
            preferencias[2][1]=preferencias[2][1]+1
        elif p.categoria=='MC':
            categoria='Muñecas y accesorios'
            preferencias[3][1]=preferencias[3][1]+1
        elif p.categoria=='FM':
            categoria='Figuras y muñecos de acción'
            preferencias[4][1]=preferencias[4][1]+1
        elif p.categoria=='PM':
            categoria='Peluches y muñecos de trapo'
            preferencias[5][1]=preferencias[5][1]+1
        elif p.categoria=='AV':
            categoria='Autos y vehículos en miniatura'
            preferencias[6][1]=preferencias[6][1]+1
        elif p.categoria=='TA':
            categoria='Trenes y accesorios'
            preferencias[7][1]=preferencias[7][1]+1
        elif p.categoria=='AA':
            categoria='Autopista y accesorios'
            preferencias[8][1]=preferencias[8][1]+1
        elif p.categoria=='HH':
            categoria='Herramientas'
            preferencias[9][1]=preferencias[9][1]+1
        elif p.categoria=='MM':
            categoria='Materiales'
            preferencias[10][1]=preferencias[10][1]+1
        elif p.categoria=='CA':
            categoria='Construcción, acoplamientos y "kits'
            preferencias[11][1]=preferencias[11][1]+1
        elif p.categoria=='IV':
            categoria='Imitación de la vida'
            preferencias[12][1]=preferencias[12][1]+1
        elif p.categoria=='MU':
            categoria='Musicales'
            preferencias[13][1]=preferencias[13][1]+1
        # elif p.categoria=='AV':
        #     categoria='Audio-visual'
        #     preferencias[14][1]=preferencias[14][1]+1
        elif p.categoria=='CT':
            categoria='Ciencia y tecnología'
            preferencias[15][1]=preferencias[15][1]+1
        elif p.categoria=='PR':
            categoria='Puzzles y rompecabezas'
            preferencias[16][1]=preferencias[16][1]+1
        elif p.categoria=='MA':
            categoria='Juego de mesa y azar'
            preferencias[17][1]=preferencias[17][1]+1
        elif p.categoria=='VO':
            categoria='Videoconsola y ordenador'
            preferencias[18][1]=preferencias[18][1]+1
        elif p.categoria=='CC':
            categoria='Comunicación'
            preferencias[19][1]=preferencias[19][1]+1
        elif p.categoria=='PH':
            categoria='Puntería y habilidad'
            preferencias[20][1]=preferencias[20][1]+1
        elif p.categoria=='AE':
            categoria='Arrastre y empuje'
            preferencias[21][1]=preferencias[21][1]+1
        elif p.categoria=='OV':
            categoria='Voladores'
            preferencias[22][1]=preferencias[22][1]+1
        elif p.categoria=='DA':
            categoria='Disfraces y accesorios'
            preferencias[23][1]=preferencias[23][1]+1
        elif p.categoria=='TT':
            categoria='Títeres y teatro'
            preferencias[24][1]=preferencias[24][1]+1
        productos.append([p.id,p.foto.url,p.nombre,p.categoria,p.estado_d,p.marca,p.modelo,p.serie,p.anno,p.tamano,p.material])

    preferencias.sort(reverse=True,key=itemgetter(1))
    response["productos"]=productos
    response["preferencias"]=preferencias

    return JsonResponse(response)

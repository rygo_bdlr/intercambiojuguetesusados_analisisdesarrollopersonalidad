from django.contrib import admin
from apps.galeria.models import Galeria

# Register your models here.
@admin.register(Galeria)
class AdminGaleria(admin.ModelAdmin):
    list_display=('infante',)

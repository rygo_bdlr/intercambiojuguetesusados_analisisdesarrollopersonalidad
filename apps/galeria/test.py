from django.test import TestCase
from django.test import Client
import unittest

from apps.usuarios.models import Usuario, Apoderado, Infante, Entidad
from apps.intercambio.models import Publicacion, DatosPublicacion, PublicacionDeseada, Solicitud, Cierre
from apps.usuarios.models import Usuario, Apoderado, Entidad, Infante
from apps.producto.models import Producto,ProductoIntercambio, Historial
from apps.galeria.models import Galeria
from apps.analisis.models import Desarrollo, Preferencias

from django.urls import reverse

import json


class GaleriaTest(TestCase):

    @classmethod
    def setUp(self):
        self.usuario_ap= Usuario.objects.create(password="password", username="user_ap", tipo="AP")
        self.usuario_in= Usuario.objects.create(password="password", username="user_in", tipo="IN")

        self.apoderado= Apoderado.objects.create(rut="1",nombre="apoderado",apellido="apellido",edad=1,movil=1,email="a@gmail.com",usuario=self.usuario_ap,foto="foto")
        self.infante= Infante.objects.create(nombre="niño",edad=1,apoderado=self.apoderado,usuario=self.usuario_in,foto="foto")
        self.galeria=Galeria.objects.create(limiteProductos=100,infante=self.infante)

        self.productoDis=Producto.objects.create(foto="foto",nombre="juguete",categoria="DV",Disponible_a_intercambio=True,QR="qr",galeria=self.galeria)
        self.productoNoDis=Producto.objects.create(foto="foto",nombre="juguete",categoria="DV",Disponible_a_intercambio=False,QR="qr",galeria=self.galeria)

    @classmethod
    def tearDown(self):
        Usuario.objects.all().delete()
        Apoderado.objects.all().delete()
        Infante.objects.all().delete()
        Producto.objects.all().delete()

    def testVerGaleria_ap(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        response = self.client.get(reverse('galeria_ap', kwargs={"id_infante": self.infante.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'galerias/galeria_apoderado.html')

    def testVerGaleria_in(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session.save()

        response = self.client.get(reverse('galeria_in', kwargs={"id_infante": self.infante.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'galerias/galeria_infante.html')

    def testConfigGaleria(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        response = self.client.post(
            reverse('galeria_config', kwargs={"id_galeria": self.galeria.pk , "limite": 10}),
            {"limiteProductos": 20}
        )

        # response = self.client.get(reverse('galeria_config', kwargs={"id_galeria": self.galeria.pk , "limite": 10}))
        self.assertEqual(response.status_code, 302)

    def testFiltrarGaleria_todo(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        response = self.client.post(
            reverse('filtrarGaleria', kwargs={"id_infante": self.infante.pk}),
            {"filtro": 'todo', }
        )

        # response = self.client.get(reverse('galeria_config', kwargs={"id_galeria": self.galeria.pk , "limite": 10}))
        self.assertEqual(response.status_code, 200)

    def testFiltrarGaleria(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        response = self.client.post(
            reverse('filtrarGaleria', kwargs={"id_infante": self.infante.pk}),
            {"filtro": 'DV', }
        )

        # response = self.client.get(reverse('galeria_config', kwargs={"id_galeria": self.galeria.pk , "limite": 10}))
        self.assertEqual(response.status_code, 200)

from django.urls import path
from apps.galeria import views

urlpatterns = [
    path('galeria_ap/<int:id_infante>', views.verGaleria_ap, name="galeria_ap"),
    path('galeria_in/<int:id_infante>', views.verGaleria_in, name="galeria_in"),
    path('filtrarGaleria/<int:id_infante>', views.filtrarGaleria, name="filtrarGaleria"),
    # path('galeria_config/<int:id_galeria>/<int:limite>', views.configGaleria, name="galeria_config"),
    path('galeria_config/<int:id_galeria>', views.configGaleria, name="galeria_config"),
]

from django.db import models
from apps.usuarios.models import Infante

# Create your models here.
class Galeria(models.Model):
    #atributos
    limiteProductos = models.PositiveIntegerField(blank=True,null=True)
    #LLaves foraneas
    infante = models.OneToOneField(Infante, on_delete=models.CASCADE, blank=True,null=True)

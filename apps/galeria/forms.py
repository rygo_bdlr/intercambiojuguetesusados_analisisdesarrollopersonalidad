from django.forms import ModelForm
from django import forms
from apps.galeria.models import Galeria

class GaleriaForm(forms.ModelForm):
    class Meta:
        model= Galeria

        fields=[
            'limiteProductos',
        ]
        labels={
            'limiteProductos':'Limite de juguetes a registrar:',
        }
        widgets={
            'limiteProductos':forms.NumberInput(attrs={'class':'form-control'}),
        }

    def save(self,commit=True):
        galeria=super(GaleriaForm,self).save(commit=False)
        if commit:
            galeria.save()
        return galeria

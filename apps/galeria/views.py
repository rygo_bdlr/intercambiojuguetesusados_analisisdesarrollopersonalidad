from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from apps.galeria.models import Galeria
from apps.usuarios.models import Apoderado, Infante
from apps.producto.models import Producto
from apps.galeria.forms import GaleriaForm

# Create your views here.
def verGaleria_ap(request,id_infante):
    data={}

    if Apoderado.objects.get(usuario_id=request.session.get('id')).rut == Infante.objects.get(id=id_infante).apoderado_id:

        data['apoderado']=Apoderado.objects.get(usuario_id=request.session.get('id'))
        data['infante']=infante=Infante.objects.get(id=id_infante)

        data['numJuguetesRegistrados']=Producto.objects.filter(galeria_id=Galeria.objects.get(infante_id=infante.id).id,Disponible_a_intercambio=True).count()
        data['numJuguetesNoRegistrados']=Producto.objects.filter(galeria_id=Galeria.objects.get(infante_id=infante.id).id,Disponible_a_intercambio=False).count()

        try:
            data['galeria']=Galeria.objects.get(infante=id_infante)
            try:
                data['productos_D']=Producto.objects.filter(galeria=Galeria.objects.get(infante=id_infante),Disponible_a_intercambio=True)
            except Producto.DoesNotExist:
                data['productos_D']=None

            try:
                data['productos_ND']=Producto.objects.filter(galeria=Galeria.objects.get(infante=id_infante),Disponible_a_intercambio=False)
            except Producto.DoesNotExist:
                data['productos_ND']=None
        except Galeria.DoesNotExist:
            data['galeria']=None
            data['productos_D']=None
            data['productos_ND']=None

        data['juguetes_num']=len(data['productos_D'])+len(data['productos_ND'])

        template_name='galerias/galeria_apoderado.html'
        return render(request,template_name,data)
    else:
        return HttpResponseRedirect(reverse('inicio_ap'))

def verGaleria_in(request,id_infante):
    data={}

    if Infante.objects.get(usuario_id=request.session.get('id')).id == id_infante:

        data['infante']=infante=Infante.objects.get(usuario_id=request.session.get('id'))
        data['numJuguetesRegistrados']=Producto.objects.filter(galeria_id=Galeria.objects.get(infante_id=infante.id).id,Disponible_a_intercambio=True).count()
        data['numJuguetesNoRegistrados']=Producto.objects.filter(galeria_id=Galeria.objects.get(infante_id=infante.id).id,Disponible_a_intercambio=False).count()

        try:
            data['galeria']=Galeria.objects.get(infante=id_infante)
            try:
                data['productos_D']=Producto.objects.filter(galeria=Galeria.objects.get(infante=id_infante),Disponible_a_intercambio=True)
            except Producto.DoesNotExist:
                data['productos_D']=None

            try:
                data['productos_ND']=Producto.objects.filter(galeria=Galeria.objects.get(infante=id_infante),Disponible_a_intercambio=False)
            except Producto.DoesNotExist:
                data['productos_ND']=None
        except Galeria.DoesNotExist:
            data['galeria']=None
            data['productos_D']=None
            data['productos_ND']=None

        data['juguetes_num']=len(data['productos_D'])+len(data['productos_ND'])


        template_name='galerias/galeria_infante.html'
        return render(request,template_name,data)
    else:
        return HttpResponseRedirect(reverse('intercambio_in'))

# def configGaleria(request, id_galeria, limite):
def configGaleria(request, id_galeria):
    data={}

    if Apoderado.objects.get(usuario_id=request.session.get('id')).rut == Infante.objects.get(id=Galeria.objects.get(id=id_galeria).infante_id).apoderado_id:

        apoderado=Apoderado.objects.get(usuario_id=request.session.get('id'))
        galeria=Galeria.objects.get(id=id_galeria)
        template_name='galerias/galeria_config.html'

        if request.method=='POST':
            limiteP = request.POST.get('limiteProductos')
            Galeria.objects.filter(id=id_galeria).update(limiteProductos=limiteP)
            return HttpResponseRedirect(reverse('galeria_config',kwargs={'id_galeria':id_galeria}))
            # return HttpResponseRedirect(reverse('galeria_config',kwargs={'id_galeria':id_galeria, 'limite':limiteP}))
        else:
            # borrar usuario
            form=GaleriaForm()

        return render(request,template_name,{'form':form, 'galeria':galeria, 'apoderado':apoderado})
    else:
        return HttpResponseRedirect(reverse('inicio_ap'))

def filtrarGaleria(request,id_infante):
    response={}
    P_disponibles=[]
    P_Nodisponibles=[]
    largoD=0
    largoND=0

    filtro=request.POST.get('filtro')

    try:
        if filtro == 'todo':
            Dis_p=Producto.objects.filter(galeria=Galeria.objects.get(infante=id_infante),Disponible_a_intercambio=True).order_by('-id')
        else:
            Dis_p=Producto.objects.filter(galeria=Galeria.objects.get(infante=id_infante),categoria=filtro,Disponible_a_intercambio=True)

    except Producto.DoesNotExist:
        Dis_p=None

    try:
        if filtro == 'todo':
            noDis_p=Producto.objects.filter(galeria=Galeria.objects.get(infante=id_infante),Disponible_a_intercambio=False).order_by('-id')
        else:
            noDis_p=Producto.objects.filter(galeria=Galeria.objects.get(infante=id_infante),categoria=filtro,Disponible_a_intercambio=False)

    except Producto.DoesNotExist:
        noDis_p=None

    for producto in Dis_p:
        largoD+=1
        P_disponibles.append([producto.pk,producto.foto.url,producto.nombre,producto.categoria])

    for producto in noDis_p:
        largoND+=1
        P_Nodisponibles.append([producto.pk,producto.foto.url,producto.nombre,producto.categoria])


    response["P_disponibles"]=P_disponibles
    response["P_Nodisponibles"]=P_Nodisponibles
    response["largoD"]=largoD
    response["largoND"]=largoND


    return JsonResponse(response)

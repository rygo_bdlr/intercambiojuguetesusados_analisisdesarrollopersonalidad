from django.contrib import admin

from apps.usuarios.models import Usuario, Apoderado, Infante, Entidad

# Register your models here.
@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    list_display=('username','password', 'tipo')

@admin.register(Apoderado)
class ApoderadoAdmin(admin.ModelAdmin):
    list_display=('rut','nombre','apellido','movil','usuario')

@admin.register(Infante)
class InfanteAdmin(admin.ModelAdmin):
    list_display=('nombre','usuario','apoderado')

@admin.register(Entidad)
class EntidadAdmin(admin.ModelAdmin):
    list_display=('nombre','usuario')

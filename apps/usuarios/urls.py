from django.urls import path
from apps.usuarios import views

urlpatterns = [
     path('', views.redireccionBienvenida, name='re_bienvenida'),
     path('bienvenido', views.Bienvenida, name='bienvenida'),
     path('informaDivertido', views.informaDivertido, name='informaDivertido'),
     path('registro_us', views.UsuarioCrear, name='registro_us'),
     path('registro_ap', views.UsuarioCrearApoderado, name='registro_ap'),
     path('registro_en', views.UsuarioCrearEntidad, name='registro_en'),
     path('registro_in', views.UsuarioCrearInfante, name='registro_in'),
     path('editar_ap/<str:id_usuario>', views.EditarApoderado, name='editar_ap'),
     path('editar_in/<int:id_infante>', views.EditarInfante, name='editar_in'),
     path('editar_en/<int:id_entidad>', views.EditarEntidad, name='editar_en'),
     path('login', views.login_usuario, name='login'),
     path('logout', views.logout_usuario, name='logout_us'),
     path('config_ap', views.config_ini_ap, name='config_ap'),
     path('inicio_ap',views.inicio_apoderado,name='inicio_ap'),
     path('inicio_en',views.inicio_entidad,name='inicio_en'),
     path('inicio_in',views.inicio_infante,name='inicio_in'),
     path('inicio_ad',views.inicio_admin,name='inicio_ad'),
     path('desactivarUsuario/<int:id_usuario>',views.desactivarUsuario,name='desactivarUsuario'),
     path('desactivarInfante/<int:id_usuario>/<int:id_apoderado>',views.desactivarInfante,name='desactivarInfante'),
     path('administrar_apoderado/<int:id_usuario>',views.administrar_apoderado,name='administrar_apoderado'),
     path('administrar_infante/<int:id_usuario>',views.administrar_infante,name='administrar_infante'),
     path('eliminar_publicacion/<int:id_juguete>/<int:id_infante>',views.eliminar_publicacion,name='eliminar_publicacion'),
     path('eliminar_juguete/<int:id_juguete>/<int:id_infante>',views.eliminar_juguete,name='eliminar_juguete'),
]

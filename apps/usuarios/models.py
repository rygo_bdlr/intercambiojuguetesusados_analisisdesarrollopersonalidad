from django.db import models
from django.contrib.auth.models import AbstractUser
from apps.usuarios.Choices import Tipo_Usuarios_1, Tipo_Usuarios_2



class Usuario(AbstractUser):
    tipo=models.CharField(max_length=2, choices= Tipo_Usuarios_1, default='AP')
    nuevo=models.BooleanField(default=True)


class Apoderado(models.Model):
    #atributos
    foto=models.ImageField(upload_to='fotoPerfil/apoderado', blank=True, null=True)
    rut=models.CharField(max_length=50, primary_key=True)
    nombre=models.CharField(max_length=20)
    apellido=models.CharField(max_length=20)
    edad = models.PositiveIntegerField(default=18)
    movil=models.IntegerField()
    email = models.EmailField()

    #LLaves foraneas
    usuario= models.OneToOneField(Usuario, on_delete=models.CASCADE, blank=True,null=True)

    # def crearApoderado(cls,rut):
    #     apoderado=cls(rut=rut)
    #     return apoderado

    def __str__(self):
        return self.nombre

class Infante(models.Model):
    #atributos
    foto=models.ImageField(upload_to='fotoPerfil/infante', blank=True, null=True)
    nombre=models.CharField(max_length=50)
    edad = models.PositiveIntegerField()

    #Llaves foraneas
    usuario= models.OneToOneField(Usuario, on_delete=models.CASCADE, blank=True,null=True)
    apoderado=models.ForeignKey(Apoderado,on_delete=models.CASCADE, blank=True,null=True)

    def __str__(self):
        return self.nombre

class Entidad(models.Model):
    #atributos
    foto=models.ImageField(upload_to='fotoPerfil/entidad', blank=True, null=True)
    nombre = models.CharField(max_length=20)
    email = models.EmailField()
    pagina = models.CharField(max_length=100,blank=True, null=True)

    #LLaves foraneas
    usuario= models.OneToOneField(Usuario, on_delete=models.CASCADE, blank=True,null=True)

    def __str__(self):
        return self.nombre

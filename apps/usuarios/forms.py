from django.forms import ModelForm
from django import forms
from django.contrib.auth.forms import UserCreationForm
from apps.usuarios.models import Usuario, Apoderado, Entidad, Infante


class SingUpForm(UserCreationForm):
    class Meta:
        model= Usuario

        fields=[
            'username',
            'password1',
            'password2',
            'tipo',
        ]
        labels={
            'username':'Usuario',
            'password1':'Contraseña',
            'password2':'Vuelva a introducir contraseña',
            'tipo':'Seleccione su tipo de usuario'

        }
        widgets={
            'username':forms.TextInput(attrs={'class':'form-control'}),
            'password1':forms.PasswordInput(attrs={'class':'form-control'}),
            'password2':forms.PasswordInput(attrs={'class':'form-control'}),
            'tipo':forms.Select(attrs={'class':'form-control'}),
        }

    def save(self,commit=True):
        user=super(SingUpForm,self).save(commit=False)

        if commit:
            user.save()
        return user

class SingUpFormInfante(UserCreationForm):
    class Meta:
        model= Usuario

        fields=[
            'username',
            'password1',
            'password2',
        ]
        labels={
            'username':'Usuario',
            'password1':'Contraseña',
            'password2':'Vuelva a introducir contraseña',

        }
        widgets={
            'username':forms.TextInput(attrs={'class':'form-control'}),
            'password1':forms.PasswordInput(attrs={'class':'form-control'}),
            'password2':forms.PasswordInput(attrs={'class':'form-control'}),
        }

    def save(self,commit=True):
        user=super(SingUpFormInfante,self).save(commit=False)

        if commit:
            user.save()
        return user

class CrearApoderado(forms.ModelForm):
    class Meta:
        model= Apoderado

        fields=[
            'foto',
            'rut',
            'nombre',
            'apellido',
            'edad',
            'movil',
            'email',
        ]
        labels={
            'foto':'Foto de Perfil (opcional)',
            'rut':'Rut',
            'nombre':'Nombre',
            'apellido':'Apellido',
            'edad':'Edad',
            'movil':'Telefono',
            'email':'Correo electronico'
        }
        widgets={
            'rut':forms.TextInput(attrs={'class':'form-control'}),
            'nombre':forms.TextInput(attrs={'class':'form-control'}),
            'apellido':forms.TextInput(attrs={'class':'form-control'}),
            'edad':forms.NumberInput(attrs={'class':'form-control'}),
            'movil':forms.NumberInput(attrs={'class':'form-control'}),
            'email':forms.EmailInput(attrs={'class':'form-control'}),
        }

    def save(self,commit=True):
        apoderado=super(CrearApoderado,self).save(commit=False)
        if commit:
            apoderado.save()
        return apoderado

class CrearEntidad(forms.ModelForm):
    class Meta:
        model= Entidad

        fields=[
            'foto',
            'nombre',
            'email',
            'pagina',
        ]
        labels={
            'foto':'Foto de Perfil (opcional)',
            'nombre':'Nombre:',
            'email':'Correo electronico:',
            'pagina':'Página web:',
        }
        widgets={
            'nombre':forms.TextInput(attrs={'class':'form-control'}),
            'email':forms.EmailInput(attrs={'class':'form-control'}),
            'pagina':forms.TextInput(attrs={'class':'form-control'}),
        }

    def save(self,commit=True):
        entidad=super(CrearEntidad,self).save(commit=False)
        if commit:
            entidad.save()
        return entidad

class CrearInfante(forms.ModelForm):
    class Meta:
        model= Infante

        fields=[
            'foto',
            'nombre',
            'edad',
        ]
        labels={
            'foto':'Foto de Perfil (opcional)',
            'nombre':'Nombre:',
            'edad':'Edad',
        }
        widgets={
            'nombre':forms.TextInput(attrs={'class':'form-control'}),
            'edad':forms.NumberInput(attrs={'class':'form-control'}),
        }

    def save(self,commit=True):
        infante=super(CrearInfante,self).save(commit=False)
        if commit:
            infante.save()
        return infante

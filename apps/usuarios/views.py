from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from django.views.generic import ListView, CreateView
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from apps.usuarios.forms import SingUpForm, SingUpFormInfante, CrearApoderado, CrearEntidad, CrearInfante
from apps.usuarios.models import Usuario, Apoderado, Entidad, Infante
from apps.galeria.models import Galeria
from apps.intercambio.models import Publicacion, DatosPublicacion, PublicacionDeseada, Solicitud
from apps.producto.models import Producto
from apps.analisis.models import Desarrollo, Preferencias


def UsuarioCrear(request):
    template_name='usuarios/registro/register_tipo_usuario.html'


    if request.method=='POST':
        form = SingUpForm(request.POST)
        if form.is_valid():
            tipo=form.cleaned_data["tipo"]

            user=form.save()
            if tipo=='AP':
                request.session['id']=user.id
                return HttpResponseRedirect(reverse('registro_ap'))

            if tipo=='EN':
                request.session['id']=user.id
                return HttpResponseRedirect(reverse('registro_en'))

    else:
        form=SingUpForm()

    return render(request,template_name,{'form':form})


def UsuarioCrearApoderado(request):
    id=request.session.get('id')
    template_name='usuarios/registro/register_apoderado.html'

    if request.method=='POST':
        form = CrearApoderado(request.POST, request.FILES)
        if form.is_valid():
            apoderado=form.save()
            if not apoderado.foto:
                # print('no foto')
                # Guardamos la ruta de la imagen por defecto
                apod = Apoderado.objects.get(rut=apoderado.rut)
                filename = '/fotoPerfil/apoderado/default.png'
                apod.foto = filename
                apod.save()

            Apoderado.objects.filter(rut=apoderado.rut).update(usuario=Usuario.objects.get(id=id))

            del request.session['id']
            return HttpResponseRedirect("login")
    else:
        # borrar usuario
        form=CrearApoderado()

    return render(request,template_name,{'form':form})

def UsuarioCrearEntidad(request):
    id=request.session.get('id')
    template_name='usuarios/registro/register_entidad.html'

    if request.method=='POST':
        form = CrearEntidad(request.POST, request.FILES)
        if form.is_valid():
            entidad=form.save()
            if not entidad.foto:
                # Guardamos la ruta de la imagen por defecto
                ent = Entidad.objects.get(id=entidad.id)
                filename = '/fotoPerfil/entidad/default.png'
                ent.foto = filename
                ent.save()

            Entidad.objects.filter(id=entidad.id).update(usuario=Usuario.objects.get(id=id))
            del request.session['id']
            return HttpResponseRedirect("login")
    else:
        form=CrearEntidad()

    return render(request,template_name,{'form':form})

def UsuarioCrearInfante(request):
    id=request.session.get('id')
    template_name='usuarios/registro/register_infante.html'

    if request.method=='POST':
        usuario_form=SingUpFormInfante(request.POST)
        infante_form=CrearInfante(request.POST, request.FILES)
        if usuario_form.is_valid() and infante_form.is_valid():
            usuario=usuario_form.save()
            Usuario.objects.filter(username=usuario.username).update(tipo='IN')

            infante=infante_form.save()
            if not infante.foto:
                # Guardamos la ruta de la imagen por defecto
                # print('no')
                inf = Infante.objects.get(id=infante.id)
                filename = '/fotoPerfil/infante/default.png'
                inf.foto = filename
                inf.save()

            Infante.objects.filter(id=infante.id).update(apoderado_id=Apoderado.objects.get(usuario_id=request.session.get('id')).rut)
            Infante.objects.filter(id=infante.id).update(usuario_id=usuario.id)

            galeria=Galeria()
            galeria.save()
            Galeria.objects.filter(id=galeria.id).update(limiteProductos=200, infante=infante.id)

            desarrolloAF=Desarrollo()
            desarrolloAF.save()
            Desarrollo.objects.filter(id=desarrolloAF.id).update(infante=infante.id)

            preferencias=Preferencias()
            preferencias.save()
            Preferencias.objects.filter(id=preferencias.id).update(infante=infante.id)

            return HttpResponseRedirect(reverse('inicio_ap'))
    else:
        usuario_form=SingUpFormInfante()
        infante_form=CrearInfante()

    return render(request,template_name,{'usuario_form':usuario_form, 'infante_form':infante_form})


def EditarApoderado(request, id_usuario):
    template_name='usuarios/apoderado/editar_apoderado.html'
    apoderado= Apoderado.objects.get(rut=id_usuario)
    if request.method=='GET':
        form=CrearApoderado(instance=apoderado)
    else:
        form = CrearApoderado(request.POST, request.FILES, instance=apoderado)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('inicio_ap'))

    return render(request, template_name,{'form':form,'apoderado':apoderado})

def EditarEntidad(request, id_entidad):
    template_name='usuarios/entidad/editar_entidad.html'
    entidad= Entidad.objects.get(id=id_entidad)
    if request.method=='GET':
        form=CrearEntidad(instance=entidad)
    else:
        form = CrearEntidad(request.POST, request.FILES, instance=entidad)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('inicio_en'))

    return render(request, template_name,{'form':form,'entidad':entidad})

def EditarInfante(request, id_infante):
    template_name='usuarios/apoderado/editar_infante.html'
    infante= Infante.objects.get(id=id_infante)
    if request.method=='GET':
        form=CrearInfante(instance=infante)
    else:
        form = CrearInfante(request.POST, request.FILES, instance=infante)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('inicio_ap'))

    return render(request, template_name,{'form':form,'infante':infante})


def login_usuario(request):
    template_name='usuarios/login_two_columns.html'
    data={}

    logout(request)
    username=password=''
    if request.POST:
        username= request.POST['username']
        password= request.POST['password']
        user = authenticate(
            username=username,
            password=password
        )

        if user is not None:
            if user.is_active:

                request.session['id']=user.id
                request.session['bienv']=True

                if user.is_superuser:
                    login(request, user)
                    return HttpResponseRedirect(reverse('inicio_ad'))

                else:

                    if user.tipo=='AP':
                            login(request, user)
                            return HttpResponseRedirect(reverse('inicio_ap'))

                    elif user.tipo=='EN':
                            login(request, user)
                            return HttpResponseRedirect(reverse('Sectores'))

                    elif user.tipo=='IN':
                            login(request, user)
                            filtro='todo'
                            return HttpResponseRedirect(reverse('intercambio_in'))
            else:
                messages.warning(
                    request,
                    'Esta cuenta ha sido desactivada'
                )
        else:
            messages.error(
                request,
                'Usuario o contraseña incorrectos!'
            )

    return render(request, template_name,data)

def logout_usuario(request):
    del request.session['id']
    logout(request)
    return HttpResponseRedirect(reverse('re_bienvenida'))

def config_ini_ap(request):
    template_name='usuarios/configuracion/conf_ini_apoderado.html'
    data={}
    return render(request, template_name,data)

def redireccionBienvenida(request):
    return HttpResponseRedirect(reverse('bienvenida'))

def Bienvenida(request):
    data={}
    template_name='bienvenida/bienvenida.html'
    return render(request,template_name,data)

def informaDivertido(request):
    try:
        usuario=Usuario.objects.get(id=request.session.get('id'))
    except Usuario.DoesNotExist:
        usuario=None

    if usuario != None:
        if usuario.tipo=='AP':
                return HttpResponseRedirect(reverse('inicio_ap'))

        elif usuario.tipo=='EN':
                return HttpResponseRedirect(reverse('Sectores'))

        elif usuario.tipo=='IN':
                return HttpResponseRedirect(reverse('inicio_in'))
    else:
        return HttpResponseRedirect(reverse('bienvenida'))

def inicio_entidad(request):
    data={}
    data['entidad']=Entidad.objects.get(usuario_id=request.session.get('id'))
    data['bienv']=request.session.get('bienv')
    request.session['bienv']=False
    template_name='analisis/analisis_en.html'
    return render(request,template_name,data)

def inicio_infante(request):
    data={}
    data['infante']=Infante.objects.get(usuario_id=request.session.get('id'))

    data['bienv']=request.session.get('bienv')
    request.session['bienv']=False
    template_name='indexs/index_infante.html'
    return render(request,template_name,data)

def inicio_apoderado(request):
    data={}
    datos=[]

    data['apoderado']=Apoderado.objects.get(usuario_id=request.session.get('id'))
    try:
        infantes=Infante.objects.filter(apoderado_id=Apoderado.objects.get(usuario_id=request.session.get('id')).rut)
        data['infantes']=infantes
    except Infante.DoesNotExist:
        infantes=None
        data['infantes']=None


    for infante in infantes:
        numJuguetesRegistrados=Producto.objects.filter(galeria_id=Galeria.objects.get(infante_id=infante.id).id,Disponible_a_intercambio=True).count()
        numJuguetesNoRegistrados=Producto.objects.filter(galeria_id=Galeria.objects.get(infante_id=infante.id).id,Disponible_a_intercambio=False).count()
        numJuguetes=numJuguetesRegistrados+numJuguetesNoRegistrados
        # Publicaciones
        num=0
        publicaciones=Publicacion.objects.filter(infante_autor_id=infante.id)
        for publicacion in publicaciones:
            datosPu=DatosPublicacion.objects.filter(publicacion_id=publicacion.id).count()

            if datosPu==0:
                num=num+1

        #Lista deseados
        data['publicaciones']=[]
        publicacionDeseada=data['publicacionesDeseadas']=PublicacionDeseada.objects.filter(infante_autor_id=infante.id)
        idPublicacion=0
        inicial=True
        final=publicacionDeseada.count()
        i=0

        for p in publicacionDeseada:
            esta=False
            for i in range(0,len(data['publicaciones'])):
                if p.publicacion_id==data['publicaciones'][i].id:
                    esta=True
            if esta==False:
                data['publicaciones'].append(Publicacion.objects.get(id=p.publicacion_id))

            # i=i+1
            # if inicial==True:
            #     idPublicacion=p.publicacion_id
            #     inicial=False
            # if p.publicacion_id !=idPublicacion:
            #     data['publicaciones'].append(Publicacion.objects.get(id=idPublicacion))
            #     productosEnPublicacion=[]
            #     idPublicacion=p.publicacion_id
            #
            # elif i==final:
            #     data['publicaciones'].append(Publicacion.objects.get(id=idPublicacion))



        numSo=0
        infAnterior=0
        for publicacionDeseada in data['publicacionesDeseadas']:
            inf=publicacionDeseada.publicacion_id
            # print("inf=",inf," anter=",infAnterior)
            if inf!=infAnterior:
                # print(publicacionDeseada, infante.nombre)
                datosSo=Solicitud.objects.filter(publicacion_deseada_id=publicacionDeseada.id,apoderado_autor=Apoderado.objects.get(usuario_id=request.session.get('id'))).count()
                # print(publicacion.id)
                if datosSo==0:
                    numSo=numSo+1
            infAnterior=inf
        # print("sig:")



        datos.append([infante,num,numSo,numJuguetesRegistrados,numJuguetesNoRegistrados,numJuguetes])

    data['datos']=datos
    # print(datos)

    data['bienv']=request.session.get('bienv')
    request.session['bienv']=False
    template_name='usuarios/apoderado/mis_ninos.html'
    return render(request,template_name,data)



# **************************** TODO ADMIN ************************************

def inicio_admin(request):
    data={}
    datos=[]

    data['admin']=Usuario.objects.get(id=request.session.get('id'))

    usuarios=Usuario.objects.all()

    for u in usuarios:
        if u.tipo=="AP" and not u.is_superuser:
            tipo=Apoderado.objects.get(usuario_id=u.id)
            datos.append([u,tipo])

        if u.tipo=="EN":
            tipo=Entidad.objects.get(usuario_id=u.id)
            datos.append([u,tipo])

    data['usuarios']=datos
    data['bienv']=request.session.get('bienv')
    request.session['bienv']=False

    template_name='indexs/index_admin.html'
    return render(request,template_name,data)


def desactivarUsuario(request, id_usuario):
    data={}

    usuario=Usuario.objects.get(id=request.session.get('id'))

    if usuario.is_superuser:


        user=Usuario.objects.get(id=id_usuario)
        if user.is_active:
            Usuario.objects.filter(id=id_usuario).update(is_active=0)
        else:
            Usuario.objects.filter(id=id_usuario).update(is_active=1)

        return HttpResponseRedirect(reverse('inicio_ad'))


    else:
        del request.session['id']
        logout(request)
        return HttpResponseRedirect(reverse('re_bienvenida'))

def desactivarInfante(request, id_usuario, id_apoderado):
    data={}

    usuario=Usuario.objects.get(id=request.session.get('id'))

    if usuario.is_superuser:


        user=Usuario.objects.get(id=id_usuario)
        if user.is_active:
            Usuario.objects.filter(id=id_usuario).update(is_active=0)
        else:
            Usuario.objects.filter(id=id_usuario).update(is_active=1)

        return HttpResponseRedirect(reverse('administrar_apoderado',kwargs={'id_usuario':id_apoderado}))


    else:
        del request.session['id']
        logout(request)
        return HttpResponseRedirect(reverse('re_bienvenida'))


def administrar_apoderado(request,id_usuario):
    data={}
    data['admin']=Usuario.objects.get(id=request.session.get('id'))
    usuario=Usuario.objects.get(id=request.session.get('id'))

    data['apoderado']=apoderado=Apoderado.objects.get(usuario_id=id_usuario)


    inf=Infante.objects.filter(apoderado_id=apoderado.rut)

    datos=[]
    for i in inf:
        usu=Usuario.objects.get(id=i.usuario.id)
        datos.append([i,usu])

    data['infantes']=datos

    template_name='usuarios/admin_ap.html'
    return render(request,template_name,data)

def administrar_infante(request,id_usuario):
    data={}
    data['admin']=Usuario.objects.get(id=request.session.get('id'))

    data['infante']=inf=Infante.objects.get(id=id_usuario)

    juguetes=Producto.objects.filter(galeria_id=Galeria.objects.get(infante_id=id_usuario).id)

    datos=[]
    for j in juguetes:
        try:
            p=Publicacion.objects.get(producto_id=j.id)
        except Publicacion.DoesNotExist:
            p=None
        datos.append([j,p])
    data['juguetes']=datos

    template_name='galerias/admin_galeria.html'
    return render(request,template_name,data)

def eliminar_publicacion(request,id_juguete, id_infante):
    data={}

    Publicacion.objects.get(producto_id=id_juguete).delete()
    return HttpResponseRedirect(reverse('administrar_infante',kwargs={'id_usuario':id_infante}))

def eliminar_juguete(request,id_juguete, id_infante):
    data={}

    Producto.objects.get(id=id_juguete).delete()
    return HttpResponseRedirect(reverse('administrar_infante',kwargs={'id_usuario':id_infante}))

from django.test import TestCase
from django.test import Client
import unittest

from apps.usuarios.models import Usuario, Apoderado, Infante, Entidad
from apps.intercambio.models import Publicacion, DatosPublicacion, PublicacionDeseada, Solicitud, Cierre
from apps.usuarios.models import Usuario, Apoderado, Entidad, Infante
from apps.producto.models import Producto,ProductoIntercambio, Historial
from apps.galeria.models import Galeria
from apps.analisis.models import Desarrollo, Preferencias

from django.urls import reverse

import json

from django.core.files.uploadedfile import SimpleUploadedFile


class UsuariosTest(TestCase):

    @classmethod
    def setUp(self):
        self.usuario_ap= Usuario.objects.create(password="password", username="user_ap", tipo="AP")
        self.usuario_ap_pass= Usuario.objects.create(password="pbkdf2_sha256$100000$NKqPXvgqOZw9$R9dJmlRhIqiTCZ30icj0CQVBD6KB26SyQlor82VLJyY=", username="user_ap_pass", tipo="AP")
        self.usuario_ap_nuevo= Usuario.objects.create(password="password", username="user_ap_nuevo", tipo="AP")
        self.usuario_in= Usuario.objects.create(password="password", username="user_in", tipo="IN")
        self.usuario_in_pass= Usuario.objects.create(password="pbkdf2_sha256$100000$NKqPXvgqOZw9$R9dJmlRhIqiTCZ30icj0CQVBD6KB26SyQlor82VLJyY=", username="user_in_pass", tipo="IN")
        self.usuario_in_nuevo= Usuario.objects.create(password="password", username="user_in_nuevo", tipo="IN")
        self.usuario_en= Usuario.objects.create(password="password", username="user_en", tipo="EN")
        self.usuario_en_pass= Usuario.objects.create(password="pbkdf2_sha256$100000$NKqPXvgqOZw9$R9dJmlRhIqiTCZ30icj0CQVBD6KB26SyQlor82VLJyY=", username="user_en_pass", tipo="EN")
        self.usuario_en_nuevo= Usuario.objects.create(password="password", username="user_en_nuevo", tipo="EN")

        self.apoderado= Apoderado.objects.create(rut="1",nombre="apoderado",apellido="apellido",edad=1,movil=1,email="a@gmail.com",usuario=self.usuario_ap,foto="foto")
        self.apoderado_pass= Apoderado.objects.create(rut="11",nombre="apoderado",apellido="apellido",edad=1,movil=1,email="a@gmail.com",usuario=self.usuario_ap_pass,foto="foto")
        self.entidad= Entidad.objects.create(nombre="entidad",email="e@gmail.com",pagina="pagina",usuario=self.usuario_en,foto="foto")
        self.entidad_pass= Entidad.objects.create(nombre="entidad",email="e@gmail.com",pagina="pagina",usuario=self.usuario_en_pass,foto="foto")
        self.infante= Infante.objects.create(nombre="niño",edad=1,apoderado=self.apoderado,usuario=self.usuario_in,foto="foto")
        self.infante_pass= Infante.objects.create(nombre="niño",edad=1,apoderado=self.apoderado,usuario=self.usuario_in_pass,foto="foto")
        self.galeria=Galeria.objects.create(limiteProductos=100,infante=self.infante)

        self.producto=Producto.objects.create(foto="foto",nombre="juguete",categoria="DV",Disponible_a_intercambio=False,QR="qr",galeria=self.galeria)
        self.historial=Historial.objects.create(producto=self.producto,Registro="test",Adquisicion="test",Intercambiado=0)

        self.publicacion=Publicacion.objects.create(Estado="Publicado",producto=self.producto,infante_autor=self.infante)
        self.datosPublicacion=DatosPublicacion.objects.create(dia="Miércoles 10 de Octubre de 2018",lugar="German Tenderini 1840, Renca, Región Metropolitana, Chile",hora="13:33",apoderado_autor=self.apoderado,publicacion=self.publicacion)

    @classmethod
    def tearDown(self):
        Usuario.objects.all().delete()
        Apoderado.objects.all().delete()
        Infante.objects.all().delete()
        Galeria.objects.all().delete()
        Producto.objects.all().delete()
        Historial.objects.all().delete()
        Publicacion.objects.all().delete()
        DatosPublicacion.objects.all().delete()

    def testUnitarioApoderado(self):
        self.assertEqual(self.apoderado.__str__(),self.apoderado.nombre)

    def testUnitarioInfante(self):
        self.assertEqual(self.infante.__str__(),self.infante.nombre)

    def testUnitarioEntidad(self):
        self.assertEqual(self.entidad.__str__(),self.entidad.nombre)

    def testUsuarioCrear(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        # foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')

        response = self.client.post(
            reverse('registro_us'),
            {"username": 'test', "password1": 'pbkdf2_sha256$100000$al0sqKuhyo0O$sJ5UX5EVAT2VEKegmX1pVWu8PX0QGQbkPeZIsEgaN/8=', "password2": 'pbkdf2_sha256$100000$al0sqKuhyo0O$sJ5UX5EVAT2VEKegmX1pVWu8PX0QGQbkPeZIsEgaN/8=', "tipo":'AP'}
        )
        self.assertEqual(response.status_code, 302)

    def testUsuarioCrearApoderado_foto(self):
        self.client.force_login(self.usuario_ap_nuevo)
        session = self.client.session
        session['id']=self.usuario_ap_nuevo.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('registro_ap'),
            {"foto": foto, "rut":'1.111.111-1', "nombre": 'apoderado', "apellido": 'test', "edad":20, "movil":11111111, "email": 'test@gmail.com'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testUsuarioCrearApoderado(self):
        self.client.force_login(self.usuario_ap_nuevo)
        session = self.client.session
        session['id']=self.usuario_ap_nuevo.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('registro_ap'),
            {"foto": None, "rut":'1.111.111-1', "nombre": 'apoderado', "apellido": 'test', "edad":20, "movil":11111111, "email": 'test@gmail.com'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testUsuarioCrearEntidad_foto(self):
        self.client.force_login(self.usuario_en_nuevo)
        session = self.client.session
        session['id']=self.usuario_en_nuevo.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('registro_en'),
            {"foto": foto, "nombre":'empresa', "email": 'test@gmail.com', "pagina": 'www.test.com'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testUsuarioCrearEntidad(self):
        self.client.force_login(self.usuario_en_nuevo)
        session = self.client.session
        session['id']=self.usuario_en_nuevo.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('registro_en'),
            {"foto": None, "nombre":'empresa', "email": 'test@gmail.com', "pagina": 'www.test.com'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testUsuarioCrearInfante_foto(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('registro_in'),
            {"username": 'test', "password1": 'pbkdf2_sha256$100000$al0sqKuhyo0O$sJ5UX5EVAT2VEKegmX1pVWu8PX0QGQbkPeZIsEgaN/8=', "password2": 'pbkdf2_sha256$100000$al0sqKuhyo0O$sJ5UX5EVAT2VEKegmX1pVWu8PX0QGQbkPeZIsEgaN/8=', "tipo":'IN', "foto": foto, "nombre":'infante', "edad":5},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testUsuarioCrearInfante(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('registro_in'),
            {"username": 'test', "password1": 'pbkdf2_sha256$100000$al0sqKuhyo0O$sJ5UX5EVAT2VEKegmX1pVWu8PX0QGQbkPeZIsEgaN/8=', "password2": 'pbkdf2_sha256$100000$al0sqKuhyo0O$sJ5UX5EVAT2VEKegmX1pVWu8PX0QGQbkPeZIsEgaN/8=', "tipo":'IN', "foto": None, "nombre":'infante', "edad":5},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testEditarApoderado(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('editar_ap',kwargs={"id_usuario": self.apoderado.pk}),
            {"foto": None, "rut":self.apoderado.pk, "nombre": 'apoderado', "apellido": 'test', "edad":20, "movil":11111111, "email": 'test@gmail.com'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testEditarEntidad(self):
        self.client.force_login(self.usuario_en)
        session = self.client.session
        session['id']=self.usuario_en.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('editar_en',kwargs={"id_entidad": self.entidad.pk}),
            {"foto": foto, "nombre":'empresa', "email": 'test@gmail.com', "pagina": 'www.test.com'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testEditarInfante(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        foto = SimpleUploadedFile(name='test.jpg', content=open("apps/producto/testImage/test.jpg", 'rb').read(), content_type='image/jpeg')
        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('editar_in',kwargs={"id_infante": self.infante.pk}),
            {"foto": None, "nombre":'infante', "edad":5},
            format='multipart'
        )
        self.assertEqual(response.status_code, 302)

    def testLogin_usuario_ap(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('login'),
            {"username": "user_ap_pass" , "password": "qwertyuiop1"}
        )
        self.assertEqual(response.status_code, 302)

    def testLogin_usuario_en(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('login'),
            {"username": "user_en_pass" , "password": "qwertyuiop1"}
        )
        self.assertEqual(response.status_code, 302)

    def testLogin_usuario_in(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.post(
            reverse('login'),
            {"username": "user_in_pass" , "password": "qwertyuiop1"}
        )
        self.assertEqual(response.status_code, 302)

    def testLogin_usuario_in(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('logout_us'))

        self.assertEqual(response.status_code, 302)

    def testConfig_ini_ap(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('config_ap'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuarios/configuracion/conf_ini_apoderado.html')

    def testRedireccionBienvenida(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('re_bienvenida'))

        self.assertEqual(response.status_code, 302)

    def testBienvenida(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('bienvenida'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'bienvenida/bienvenida.html')

    def testInformaDivertido_ap(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('informaDivertido'))

        self.assertEqual(response.status_code, 302)

    def testInformaDivertido_en(self):
        self.client.force_login(self.usuario_en)
        session = self.client.session
        session['id']=self.usuario_en.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('informaDivertido'))

        self.assertEqual(response.status_code, 302)

    def testInformaDivertido_in(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('informaDivertido'))

        self.assertEqual(response.status_code, 302)

    def testInicio_entidad(self):
        self.client.force_login(self.usuario_en)
        session = self.client.session
        session['id']=self.usuario_en.id
        session['bienv']=True
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('inicio_en'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'analisis/analisis_en.html')

    def testInicio_infante(self):
        self.client.force_login(self.usuario_in)
        session = self.client.session
        session['id']=self.usuario_in.id
        session['bienv']=True
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('inicio_in'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'indexs/index_infante.html')

    def testInicio_apoderado(self):
        self.client.force_login(self.usuario_ap)
        session = self.client.session
        session['id']=self.usuario_ap.id
        session['bienv']=True
        session.save()

        # response = self.client.get(reverse('producto_reg',kwargs={"id_galeria": self.galeria.pk}))
        response = self.client.get(reverse('inicio_ap'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuarios/apoderado/mis_ninos.html')
